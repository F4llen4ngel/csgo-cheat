#include "patch.h"

#include "general.h"
#include "smbios.h"
#include "utils.h"

void PrintSMBIOSString(SMBIOS_STRUCTURE_POINTER table, SMBIOS_STRING* field) {
    if (!table.Raw || !field)
        return;

    UINT8 index = 1;
    char* astr = (char*)(table.Raw + table.Hdr->Length);
    while (index != *field) {
        if (*astr) {
            index++;
        }

        while (*astr != 0)
            astr++;
        astr++;

        if (*astr == 0) {
            if (*field == 0) {
                astr[1] = 0;
            }

            *field = index;

            if (index == 1) {
                astr--;
            }
            break;
        }
    }
    Print(L"%s", astr);
}

void EditRandom(SMBIOS_STRUCTURE_POINTER table, SMBIOS_STRING* field) {
    char buffer[258];
    RandomText(buffer, 257);

    if (field) {
        EditString(table, field, buffer);
    }
}

void PatchType0(SMBIOS_STRUCTURE_TABLE* entry) {
    SMBIOS_STRUCTURE_POINTER table =
        FindTableByType(entry, SMBIOS_TYPE_BIOS_INFORMATION, 0);
    Print(L"[WORK] Patching type0 table at 0x%08x...\n", table);

    if (!table.Type0) {
        Print(L"[FAIL] Table is non existent\n");
        return;
    }

    EditRandom(table, &table.Type0->Vendor);
    EditRandom(table, &table.Type0->BiosVersion);
    EditRandom(table, &table.Type0->BiosReleaseDate);

    Print(L"[INFO] Patched type0 table\n");
}

void PatchType1(SMBIOS_STRUCTURE_TABLE* entry) {
    SMBIOS_STRUCTURE_POINTER table =
        FindTableByType(entry, SMBIOS_TYPE_SYSTEM_INFORMATION, 0);
    Print(L"[WORK] Patching type1 table at 0x%08x...\n", table);

    if (!table.Type1) {
        Print(L"[FAIL] Table is non existent\n");
        return;
    }

    EditRandom(table, &table.Type1->Manufacturer);
    EditRandom(table, &table.Type1->ProductName);
    EditRandom(table, &table.Type1->Version);
    EditRandom(table, &table.Type1->SerialNumber);

    table.Type1->Uuid.Data1 = (UINT32)RandomNumber(10, 200000000);
    table.Type1->Uuid.Data2 = (UINT16)RandomNumber(10, 60000);
    table.Type1->Uuid.Data3 = (UINT16)RandomNumber(10, 60000);
    for (int i = 0; i < 8; i++) {
        table.Type1->Uuid.Data4[i] = (UINT8)RandomNumber(10, 250);
    }

    Print(L"[INFO] Patched type1 table\n");
}

void PatchType2(SMBIOS_STRUCTURE_TABLE* entry) {
    SMBIOS_STRUCTURE_POINTER table =
        FindTableByType(entry, SMBIOS_TYPE_BASEBOARD_INFORMATION, 0);
    Print(L"[WORK] Patching type2 table at 0x%08x...\n", table);

    if (!table.Type2) {
        Print(L"[FAIL] Table is non existent\n");
        return;
    }

    EditRandom(table, &table.Type2->Manufacturer);
    EditRandom(table, &table.Type2->ProductName);
    EditRandom(table, &table.Type2->Version);
    EditRandom(table, &table.Type2->SerialNumber);

    Print(L"[INFO] Patched type2 table\n");
}

void PatchType3(SMBIOS_STRUCTURE_TABLE* entry) {
    SMBIOS_STRUCTURE_POINTER table =
        FindTableByType(entry, SMBIOS_TYPE_SYSTEM_ENCLOSURE, 0);
    Print(L"[WORK] Patching type3 table at 0x%08x...\n", table);

    if (!table.Type3) {
        Print(L"[FAIL] Table is non existent\n");
        return;
    }

    EditRandom(table, &table.Type3->Manufacturer);
    EditRandom(table, &table.Type3->Version);
    EditRandom(table, &table.Type3->SerialNumber);
    EditRandom(table, &table.Type3->AssetTag);

    Print(L"[INFO] Patched type3 table\n");
}

void PatchType4(SMBIOS_STRUCTURE_TABLE* entry) {
    SMBIOS_STRUCTURE_POINTER table =
        FindTableByType(entry, SMBIOS_TYPE_PROCESSOR_INFORMATION, 0);
    Print(L"[WORK] Patching type4 table at 0x%08x...\n", table);

    if (!table.Type4) {
        Print(L"[FAIL] Table is non existent\n");
        return;
    }
    EditRandom(table, &table.Type4->ProcessorManufacture);
    EditRandom(table, &table.Type4->ProcessorVersion);

    Print(L"[INFO] Patched type4 table\n");
}

void PatchType17(SMBIOS_STRUCTURE_TABLE* entry) {
    SMBIOS_STRUCTURE_POINTER smbiosTable;
    smbiosTable.Raw = (UINT8*)((UINTN)entry->TableAddress);
    if (!smbiosTable.Raw) {
        Print(L"[FAIL] SMBIOS table is non existent\n");
        return;
    }
    while (smbiosTable.Hdr->Type != SMBIOS_TYPE_END_OF_TABLE) {
        if (smbiosTable.Hdr->Type == 17) {
            if (smbiosTable.Type17) {
                Print(L"[INFO] Pathing memory stick with size %d MB\n", smbiosTable.Type17->Size);
                EditRandom(smbiosTable, &smbiosTable.Type17->Manufacturer);
                EditRandom(smbiosTable, &smbiosTable.Type17->SerialNumber);
            }
        }
        smbiosTable.Raw = (UINT8*)(smbiosTable.Raw + TableLenght(smbiosTable));
    }
    Print(L"[INFO] Patched type17 table\n");
}

void PatchAll(SMBIOS_STRUCTURE_TABLE* entry) {
    PatchType0(entry);
    PatchType1(entry);
    PatchType2(entry);
    PatchType3(entry);
    PatchType4(entry);
    PatchType17(entry);
}