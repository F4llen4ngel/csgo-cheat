#include <iostream>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "cheat_engine.h"
#include "hardware.h"
#include "SimpleIni.h"
#include <stdio.h>
#include <strsafe.h>
#include <process.h> 
#include <tchar.h>
#include <GL/gl3w.h>      
#include <GLFW/glfw3.h>
#include "other.h"

#define DEBUG 0
//#define RAYTRACE 1

std::map<int, std::string> weaponsnames = {
	{weapon_deagle, "Deagle"},
	{weapon_ak47, "Ak-47"},
	{weapon_awp, "AWP"},
	{weapon_usp, "USP-S"},
	{weapon_glock, "Glock"},
	{weapon_m4a1, "M4A4"},
	{weapon_m4a1s, "M4A1-s"},
	{weapon_cz75_auto, "CZ75"},
	{weapon_r8_revolver, "R8 Revolver"},
	{weapon_elite, "Dual berettas"},
	{weapon_fiveseven, "Five-seven"},
	{weapon_hkp2000, "P2000"},
	{weapon_p250, "P250"},
	{weapon_tec9, "Tec9"},
	{weapon_aug, "AUG"},
	{weapon_famas, "Famas"},
	{weapon_g3sg1, "G3SG1"},
	{weapon_galilar, "Galil"},
	{weapon_scar20, "Scar20"},
	{weapon_sg556, "SG556"},
	{weapon_ssg08, "SSG08"},
	{weapon_mac10, "Mac10"},
	{weapon_mp7, "MP7"},
	{weapon_mp9, "MP9"},
	{weapon_bizon, "Bizon"},
	{weapon_p90, "P90"},
	{weapon_ump45, "UMP45"},
	{weapon_m249, "M249"},
	{weapon_negev, "Negev"},
	{weapon_sawedoff, "SawedOff"},
	{weapon_xm1014, "XM1014"},
	{weapon_nova, "Nova"},
	{weapon_mag7, "Mag7"},
	{weapon_tmp, "MP5-SD"}
};

std::map<int, const char*> Keys = {
	{0x01, "Left mouse button"},
	{0x02, "Right mouse button"},
	{0x03, "Control-break processing"},
	{0x04, "Middle mouse button"},
	{0x05, "X1 mouse button"},
	{0x06, "X2 mouse button"},
	{0x08, "BACKSPACE"},
	{0x09, "TAB"},
	{0x0C, "CLEAR"},
	{0x0D, "ENTER"},
	{0x10, "SHIFT"},
	{0x11, "CTRL"},
	{0x12, "ALT"},
	{0x13, "PAUSE"},
	{0x14, "CAPS LOCK"},
	{0x1B, "ESC"},
	{0x20, "SPACEBAR"},
	{0x21, "PAGE UP"},
	{0x22, "PAGE DOWN"},
	{0x23, "END"},
	{0x24, "HOME"},
	{0x25, "LEFT ARROW"},
	{0x26, "UP ARROW"},
	{0x27, "RIGHT ARROW"},
	{0x28, "DOWN ARROW"},
	{0x29, "SELECT key"},
	{0x2A, "PRINT"},
	{0x2B, "EXECUTE"},
	{0x2C, "PRINT SCREEN"},
	{0x2D, "INS key"},
	{0x2E, "DEL key"},
	{0x2F, "HELP key"},
	{0x30, "0"},
	{0x31, "1"},
	{0x32, "2"},
	{0x33, "3"},
	{0x34, "4"},
	{0x35, "5"},
	{0x36, "6"},
	{0x37, "7"},
	{0x38, "8"},
	{0x39, "9"},
	{0x41, "A"},
	{0x42, "B"},
	{0x43, "C"},
	{0x44, "D"},
	{0x45, "E"},
	{0x46, "F"},
	{0x47, "G"},
	{0x48, "H"},
	{0x49, "I"},
	{0x4A, "J"},
	{0x4B, "K"},
	{0x4C, "L"},
	{0x4D, "M"},
	{0x4E, "N"},
	{0x4F, "O"},
	{0x50, "P"},
	{0x51, "Q"},
	{0x52, "R"},
	{0x53, "S"},
	{0x54, "T"},
	{0x55, "U"},
	{0x56, "V"},
	{0x57, "W"},
	{0x58, "X"},
	{0x59, "Y"},
	{0x5A, "Z"},
	{0x5B, "Left Windows key"},
	{0x5C, "Right Windows key"},
	{0x5D, "Applications key"},
	{0x5F, "Computer Sleep key"},
	{0x60, "Numeric keypad 0 key"},
	{0x61, "Numeric keypad 1 key"},
	{0x62, "Numeric keypad 2 key"},
	{0x63, "Numeric keypad 3 key"},
	{0x64, "Numeric keypad 4 key"},
	{0x65, "Numeric keypad 5 key"},
	{0x66, "Numeric keypad 6 key"},
	{0x67, "Numeric keypad 7 key"},
	{0x68, "Numeric keypad 8 key"},
	{0x69, "Numeric keypad 9 key"},
	{0x6A, "Multiply key"},
	{0x6B, "Add key"},
	{0x6C, "Separator key"},
	{0x6D, "Subtract key"},
	{0x6E, "Decimal key"},
	{0x6F, "Divide key"},
	{0x70, "F1"},
	{0x71, "F2"},
	{0x72, "F3"},
	{0x73, "F4"},
	{0x74, "F5"},
	{0x75, "F6"},
	{0x76, "F7"},
	{0x77, "F8"},
	{0x78, "F9"},
	{0x79, "F10"},
	{0x7A, "F11"},
	{0x7B, "F12"},
	{0x7C, "F13"},
	{0x7D, "F14"},
	{0x7E, "F15"},
	{0x7F, "F16"},
	{0x80, "F17"},
	{0x81, "F18"},
	{0x82, "F19"},
	{0x83, "F20"},
	{0x84, "F21"},
	{0x85, "F22"},
	{0x86, "F23"},
	{0x87, "F24"},
	{0x90, "NUM LOCK key"},
	{0x91, "SCROLL LOCK key"},
	{0xA0, "Left SHIFT key"},
	{0xA1, "Right SHIFT key"},
	{0xA2, "Left CONTROL key"},
	{0xA3, "Right CONTROL key"},
	{0xA4, "Left MENU key"},
	{0xA5, "Right MENU key"}
};

const char* AimTargets[] = { "Head", "Body", "Neck" };
int AimTargetsID[3] = { 8, 6, 7 };

#define GLFW_EXPOSE_NATIVE_WIN32

#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

#define PI 3.14159265358979323846f

cheat_engine game;
hardware output;
other helpers;

bool running = true;

bool trigger = false;
bool AIMBOT = false;
bool RCS = false;
bool WallHack = false;
bool Chams = false;
bool antiFlash = false;
bool GrenadePredict = false;
bool skinChanger = false;
bool maphack = false;
bool autoBhop = false;
bool nightMode = false;

// skinchanger config
int CONFIG_PaintKit[65];
int CONFIG_StatTrak[65];
int CONFIG_Quality[65];
float CONFIG_Wear[65];
int CONFIG_Seed[65];

//Config system

int config_id = 0;
const char* configs_list[512];

float C_AIMRCS = 5;
float C_AIMRCS_R = 1;
float C_AIMRCS_R_LOCK = 1;
bool C_LOCK_ON_LISTED_WEAPONS = 0;
bool C_AUTOSPRAY_ON_LISTED_WEAPONS = 0;
bool C_SCALE_BY_DISTANCE = 1;
bool UseRayTracingLock = 1;
float PlayerFOV = 110.0;
int TriggerBotSleepTimeMs = 50;
bool TriggerBotToggle = false;
int TriggerBotKeyCode = 0;
float ChamsBrightness = 4.f;
float NightModeBrightnessFix = 1.f;
int ForceUpdateKeyCode = 0;
ImVec4 WHcolor;
ImVec4 Chamscolor;
static bool SprayWeapons[65];
static bool LockWeapons[65];
static int SelectedAimID = 0;
float NightmodeExposureValue = 0.075f;


#pragma region Main_Funcs

void TriggerBot() {
	game.updateEntityList();
	game.updateLocalPlayer();
	int myHealth = game.getHealthByEntity(game.localPlayer);
	int myTeam = game.getTeamByEntity(game.localPlayer);
	if (myHealth > 0) {
		short weaponID = game.getWeaponIdByEntity(game.localPlayer);
		BOOL isScoped = game.isEntityScoped(game.localPlayer);
		DWORD crosshairEntity = game.getEntityInScope(game.localPlayer);
		int crosshairEntityTeam = game.getTeamByEntity(crosshairEntity);
		int crosshairEntityHealth = game.getHealthByEntity(crosshairEntity);
		if (crosshairEntityTeam > 0
			&& crosshairEntityHealth > 0
			&& (myTeam != crosshairEntityTeam || game.DangerZone)) {

			if (GetAsyncKeyState(TriggerBotKeyCode) || TriggerBotToggle) {
				Sleep(TriggerBotSleepTimeMs);
				output.MouseLeft();
			}

			if (SprayWeapons[weaponID] && C_AUTOSPRAY_ON_LISTED_WEAPONS) {
				game.startSpaying();
			} else {
				game.stopSpraying();
			}
		}
	}
}

inline Vector3 Smooth(float smooth, Vector3 currentAngle, Vector3 aimAngle) {

	Vector3 angle = aimAngle;
	Vector3 dest = angle;

	if (smooth > 0) {

		Vector3 delta = currentAngle - aimAngle;

		delta.Normalize();

		dest = currentAngle - delta / smooth;

		dest.Normalize();

	}

	angle = dest;

	return angle;

}

void RecoilControlSystem() {

	game.updateLocalPlayer();

	Vector3 cur_punch = game.getVecPunch(game.localPlayer) * 2;

	if (GetAsyncKeyState(VK_LBUTTON) && game.ShotsFiredByEntity(game.localPlayer) > 1) {

		Vector3 viewAngle = game.getViewAngles();

		viewAngle = viewAngle + game.oldPunch - cur_punch;
		viewAngle.Normalize();

		game.writeViewAngles(viewAngle);

	}

	game.oldPunch = cur_punch;

}

void AimBot() {

	game.updateEntityList();
	game.updateLocalPlayer();

	short weapon = game.getWeaponIdByEntity(game.localPlayer);
	BOOL isScoped = game.isEntityScoped(game.localPlayer);
	BOOL Lock = false;

	game.aimBotSmooth = C_AIMRCS;
	// TODO: Listed weapons in cheat
	if (LockWeapons[weapon] && C_LOCK_ON_LISTED_WEAPONS) {
		Lock = true;
		game.aim_r = C_AIMRCS_R_LOCK;
	} else {
		game.aim_r = C_AIMRCS_R;
	}

	Vector3 viewAngle = game.getViewAngles();
	Vector3 cur_punch = game.getVecPunch(game.localPlayer) * 2.f;

	DWORD bestEnemy = game.GetClosestEnemy(viewAngle, cur_punch, AimTargetsID[SelectedAimID], UseRayTracingLock);

	if (bestEnemy != 666 && (game.isWeaponPistol(weapon) || game.isWeaponSniper(weapon) || game.isWeaponRifle(weapon)) && game.canShoot(game.localPlayer)) {

		Vector3 enemyHead = game.getBonePosByEntity(bestEnemy, AimTargetsID[SelectedAimID]);

		if (C_SCALE_BY_DISTANCE)
			game.aim_r = game.aim_r * game.scaleAimFOVByDistance(enemyHead);

		Vector3 AimAngle = game.CalcAngle(game.getEyePositionByEntity(game.localPlayer), enemyHead) - cur_punch;

		if (game.IsInRadius(viewAngle, AimAngle) && (GetAsyncKeyState(VK_LBUTTON) || Lock)) {

			AimAngle = Smooth(C_AIMRCS, viewAngle, AimAngle);
			AimAngle.Normalize();
			game.writeViewAngles(AimAngle);

		} else {
			game.stopSpraying();
		}

	} else {
		game.stopSpraying();
	}

	game.oldPunch = cur_punch;

}

void WH() {
	game.updateEntityList();
	game.updateLocalPlayer();
	for (int i = 0; i < 64; i++) {
		DWORD entity = game.getEntityById(i);
		if (!entity)
			continue;
		if (game.getHealthByEntity(entity) <= 0 || (game.getTeamByEntity(entity) == game.getTeamByEntity(game.localPlayer) && !game.DangerZone))
			continue;

		int glowIndex = game.getEntityGlowIndex(entity);
		game.makeGlow(glowIndex, WHcolor.x, WHcolor.y, WHcolor.z, 1.f);
	}
}

void ChamsFunc(BOOL clear = false) {
	game.updateEntityList();
	game.updateLocalPlayer();
	for (int i = 0; i < 64; i++) {
		DWORD entity = game.getEntityById(i);
		if (!entity)
			continue;
		if (game.getHealthByEntity(entity) <= 0 || (game.getTeamByEntity(entity) == game.getTeamByEntity(game.localPlayer) && !game.DangerZone))
			continue;


		Vector3 color;
		if (clear)
			color = { 0, 0, 0 };
		else
			color = { 255 * Chamscolor.x, 255 * Chamscolor.y, 255 * Chamscolor.z };

		game.setModelColor(entity, color);
	}
	if (clear)
		game.setModelsBrightness(0.f);
	else
		game.setModelsBrightness(ChamsBrightness * NightModeBrightnessFix);
}

void AntiFlash() {
	int CurrentFlashDuration = game.getFlashDuration(game.localPlayer);
	if (CurrentFlashDuration != 0) {
		game.FlashedUntil = game.millis() + 5000;
		game.setFlashDuration(game.localPlayer, 0);
	}
}

void MapFunc(BOOL clear = false) {
	game.updateEntityList();
	for (int i = 0; i < 64; i++) {
		DWORD entity = game.getEntityById(i);
		if (!entity)
			continue;
		if (game.getHealthByEntity(entity) <= 0 || (game.getTeamByEntity(entity) == game.getTeamByEntity(game.localPlayer) && !game.DangerZone))
			continue;

		game.isSpotted(entity);
	}
}

void SkinChanger() {
	game.updateEntityList();
	game.updateLocalPlayer();
	for (int j = 0; j < 8; ++j) {
		UINT current_PaintKit;
		int current_StatTrak;
		int current_Quality;
		float current_Wear;
		int current_Seed;

		DWORD weaponBase = game.GetWeaponBase(game.localPlayer, j);
		short id = game.getWeaponIdByEntity(game.localPlayer, j);
		int accountID = game.GetAccountId(weaponBase);

		if (id > 0 && id <= 64) {
			current_PaintKit = CONFIG_PaintKit[id];
			current_Wear = CONFIG_Wear[id];
			current_StatTrak = CONFIG_StatTrak[id];
			current_Quality = CONFIG_Quality[id];
			current_Seed = CONFIG_Seed[id];
			int curPaintKit = game.GetCurrentPaintKit(weaponBase);
			if (curPaintKit != current_PaintKit && current_PaintKit != 0) {
				game.WriteSkinData(weaponBase, current_PaintKit, current_StatTrak, current_Wear, current_Quality, accountID, current_Seed);
			}
		}
	}
}

void SetFov() {
	game.updateLocalPlayer();
	BOOL isScoped = game.isEntityScoped(game.localPlayer);
	if (!isScoped)
		game.setlLocalPlayerViewModelFov(PlayerFOV);
}

void ForceUpdateAll() {
	game.force_update_all();
}

void BHOP() {
	game.updateLocalPlayer();
	if (!game.localPlayer)
		return;
	BYTE flag = game.getPlayerJumpFlag(game.localPlayer);
	if (GetAsyncKeyState(VK_SPACE) && (flag & (1 << 0))) {
		game.forceJump();
	}
}

void NightMode(bool status) {

	for (int i = 0; i < 2048; ++i) {

		DWORD entity = game.getEntityById(i);

		if (!entity)
			continue;

		int classID = game.getClassID(entity);

		if (classID != CEnvTonemapController)
			continue;

		if (status) {
			game.activateNightMode(entity, NightmodeExposureValue);
		} else {
			game.deactivateNightMode(entity);
		}

	}
}

void InitConfig() {

	CSimpleIniA ini;
	ini.SetUnicode();

	ini.LoadFile("cfg.ini");

	char cfg_str[80];

	for (size_t i = 0; i < 80; ++i) {
		cfg_str[i] = '0';
	}

	ini.SetValue("CH", "first", "false");

	ini.SetValue("CH", "trigger", "0");
	ini.SetValue("CH", "AIMBOT", "0");
	ini.SetValue("CH", "RCS", "0");
	ini.SetValue("CH", "WallHack", "0");
	ini.SetValue("CH", "Chams", "0");
	ini.SetValue("CH", "antiFlash", "0");
	ini.SetValue("CH", "GrenadePredict", "0");
	ini.SetValue("CH", "skinChanger", "0");
	ini.SetValue("CH", "maphack", "0");
	ini.SetValue("CH", "autoBhop", "0");
	ini.SetValue("CH", "nightMode", "0");

	ini.SetValue("CH", "C_AIMRCS", "5");

	ini.SetValue("CH", "C_AIMRCS_R", "10");

	ini.SetValue("CH", "C_AIMRCS_R_LOCK", "10");

	ini.SetValue("CH", "C_LOCK_ON_LISTED_WEAPONS", "0");

	ini.SetValue("CH", "C_AUTOSPRAY_ON_LISTED_WEAPONS", "0");

	ini.SetValue("CH", "C_SCALE_BY_DISTANCE", "1");

	ini.SetValue("CH", "UseRayTracingLock", "1");

	ini.SetValue("CH", "ScreenWidth", "1920"); // we dont need it anymore, do we?
	ini.SetValue("CH", "ScreenHeight", "1080");

	ini.SetValue("CH", "PlayerFOV", "100");

	ini.SetValue("CH", "TriggerBotSleepTimeMs", "0");

	ini.SetValue("CH", "TriggerBotToggle", "0");

	ini.SetValue("CH", "TriggerBotKeyCode", "0");

	ini.SetValue("CH", "ChamsBrightness", "5");

	ini.SetValue("CH", "ForceUpdateKeyCode", "0");

	ini.SetValue("CH", "NightmodeExposureValue", "0.075");


	float
		WHcolorX = WHcolor.x,
		WHcolorY = WHcolor.y,
		WHcolorZ = WHcolor.z,
		WHcolorW = WHcolor.w;

	ini.SetValue("CH", "WHcolorX", "0");

	ini.SetValue("CH", "WHcolorY", "0");

	ini.SetValue("CH", "WHcolorZ", "5");

	ini.SetValue("CH", "WHcolorW", "0");

	float
		ChamscolorX = Chamscolor.x,
		ChamscolorY = Chamscolor.y,
		ChamscolorZ = Chamscolor.z,
		ChamscolorW = Chamscolor.w;

	ini.SetValue("CH", "ChamscolorX", "5");

	ini.SetValue("CH", "ChamscolorY", "0");

	ini.SetValue("CH", "ChamscolorZ", "0");

	ini.SetValue("CH", "ChamscolorW", "0");

	ini.SetValue("CH", "SprayWeapons", cfg_str);

	ini.SetValue("CH", "LockWeapons", cfg_str);

	ini.SetValue("CH", "SelectedAimID", 0);

	std::string tmp;

	for (pair<int, std::string> weapon : weaponsnames) {

		tmp = "PaintKit" + weapon.second;

		ini.SetValue("CH", tmp.c_str(), "0");

		tmp = "StatTrak" + weapon.second;

		ini.SetValue("CH", tmp.c_str(), "0");

		tmp = "Quality" + weapon.second;

		ini.SetValue("CH", tmp.c_str(), "0");

		tmp = "Wear" + weapon.second;

		ini.SetValue("CH", tmp.c_str(), "0");

		tmp = "Seed" + weapon.second;

		ini.SetValue("CH", tmp.c_str(), "0");

	}

	ini.SaveFile("cfg.ini");

}

void ConfigLoad(std::string configName) {

	CSimpleIniA ini;
	ini.SetUnicode();

	ini.LoadFile(configName.c_str());

	trigger = atoi(ini.GetValue("CH", "trigger", ""));
	AIMBOT = atoi(ini.GetValue("CH", "AIMBOT", ""));
	RCS = atoi(ini.GetValue("CH", "RCS", ""));
	WallHack = atoi(ini.GetValue("CH", "WallHack", ""));
	Chams = atoi(ini.GetValue("CH", "Chams", ""));
	antiFlash = atoi(ini.GetValue("CH", "antiFlash", ""));
	GrenadePredict = atoi(ini.GetValue("CH", "GrenadePredict", ""));
	skinChanger = atoi(ini.GetValue("CH", "skinChanger", ""));
	maphack = atoi(ini.GetValue("CH", "maphack", ""));
	autoBhop = atoi(ini.GetValue("CH", "autoBhop", ""));
	nightMode = atoi(ini.GetValue("CH", "nightMode", ""));

	C_AIMRCS = atof(ini.GetValue("CH", "C_AIMRCS", ""));
	C_AIMRCS_R = atof(ini.GetValue("CH", "C_AIMRCS_R", ""));
	C_AIMRCS_R_LOCK = atof(ini.GetValue("CH", "C_AIMRCS_R_LOCK", ""));
	C_LOCK_ON_LISTED_WEAPONS = atoi(ini.GetValue("CH", "C_LOCK_ON_LISTED_WEAPONS", ""));
	C_AUTOSPRAY_ON_LISTED_WEAPONS = atoi(ini.GetValue("CH", "C_AUTOSPRAY_ON_LISTED_WEAPONS", ""));
	C_SCALE_BY_DISTANCE = atoi(ini.GetValue("CH", "C_SCALE_BY_DISTANCE", ""));
	game.ScreenWidth = atoi(ini.GetValue("CH", "ScreenWidth", ""));
	game.ScreenHeight = atoi(ini.GetValue("CH", "ScreenHeight", ""));
	PlayerFOV = atof(ini.GetValue("CH", "PlayerFOV", ""));
	UseRayTracingLock = atoi(ini.GetValue("CH", "UseRayTracingLock", ""));
	TriggerBotSleepTimeMs = atoi(ini.GetValue("CH", "TriggerBotSleepTimeMs", ""));
	TriggerBotToggle = atoi(ini.GetValue("CH", "TriggerBotToggle", ""));
	TriggerBotKeyCode = atoi(ini.GetValue("CH", "TriggerBotKeyCode", ""));
	ChamsBrightness = atof(ini.GetValue("CH", "ChamsBrightness", ""));
	ForceUpdateKeyCode = atoi(ini.GetValue("CH", "ForceUpdateKeyCode", ""));

	float WHcolorX, WHcolorY, WHcolorZ, WHcolorW;
	WHcolorX = atof(ini.GetValue("CH", "WHcolorX", ""));
	WHcolorY = atof(ini.GetValue("CH", "WHcolorY", ""));
	WHcolorZ = atof(ini.GetValue("CH", "WHcolorZ", ""));
	WHcolorW = atof(ini.GetValue("CH", "WHcolorW", ""));
	WHcolor.x = WHcolorX;
	WHcolor.y = WHcolorY;
	WHcolor.z = WHcolorZ;
	WHcolor.w = WHcolorW;

	float ChamscolorX, ChamscolorY, ChamscolorZ, ChamscolorW;
	ChamscolorX = atof(ini.GetValue("CH", "ChamscolorX", ""));
	ChamscolorY = atof(ini.GetValue("CH", "ChamscolorY", ""));
	ChamscolorZ = atof(ini.GetValue("CH", "ChamscolorZ", ""));
	ChamscolorW = atof(ini.GetValue("CH", "ChamscolorW", ""));
	Chamscolor.x = ChamscolorX;
	Chamscolor.y = ChamscolorY;
	Chamscolor.z = ChamscolorZ;
	Chamscolor.w = ChamscolorW;

	std::string cfg_str = ini.GetValue("CH", "SprayWeapons", "");

	for (size_t i = 0; i < 65; ++i) {
		SprayWeapons[i] = cfg_str[i] - '0';
	}

	cfg_str = ini.GetValue("CH", "LockWeapons", "");

	for (size_t i = 0; i < 65; ++i) {
		LockWeapons[i] = cfg_str[i] - '0';
	}

	SelectedAimID = atoi(ini.GetValue("CH", "SelectedAimID", ""));
	NightmodeExposureValue = atof(ini.GetValue("CH", "NightmodeExposureValue", ""));

	std::string tmp;

	for (pair<int, std::string> weapon : weaponsnames) {

		tmp = "PaintKit" + weapon.second;

		CONFIG_PaintKit[weapon.first] = atoi(ini.GetValue("CH", tmp.c_str(), ""));

		tmp = "StatTrak" + weapon.second;

		CONFIG_StatTrak[weapon.first] = atoi(ini.GetValue("CH", tmp.c_str(), ""));

		tmp = "Quality" + weapon.second;

		CONFIG_Quality[weapon.first] = atoi(ini.GetValue("CH", tmp.c_str(), ""));

		tmp = "Wear" + weapon.second;

		CONFIG_Wear[weapon.first] = atof(ini.GetValue("CH", tmp.c_str(), ""));

		tmp = "Seed" + weapon.second;

		CONFIG_Seed[weapon.first] = atoi(ini.GetValue("CH", tmp.c_str(), ""));

	}

}

void ConfigSave(std::string configName) {

	CSimpleIniA ini;
	ini.SetUnicode();

	ini.LoadFile(configName.c_str());

	char cfg_str[80];

	for (size_t i = 0; i < 80; ++i) {
		cfg_str[i] = '0';
	}

	ini.SetValue("CH", "first", "false");

	sprintf(cfg_str, "%d", trigger);
	ini.SetValue("CH", "trigger", cfg_str);

	sprintf(cfg_str, "%d", AIMBOT);
	ini.SetValue("CH", "AIMBOT", cfg_str);

	sprintf(cfg_str, "%d", RCS);
	ini.SetValue("CH", "RCS", cfg_str);

	sprintf(cfg_str, "%d", WallHack);
	ini.SetValue("CH", "WallHack", cfg_str);

	sprintf(cfg_str, "%d", Chams);
	ini.SetValue("CH", "Chams", cfg_str);

	sprintf(cfg_str, "%d", antiFlash);
	ini.SetValue("CH", "antiFlash", cfg_str);

	sprintf(cfg_str, "%d", GrenadePredict);
	ini.SetValue("CH", "GrenadePredict", cfg_str);

	sprintf(cfg_str, "%d", skinChanger);
	ini.SetValue("CH", "skinChanger", cfg_str);

	sprintf(cfg_str, "%d", maphack);
	ini.SetValue("CH", "maphack", cfg_str);

	sprintf(cfg_str, "%d", autoBhop);
	ini.SetValue("CH", "autoBhop", cfg_str);

	sprintf(cfg_str, "%d", nightMode);
	ini.SetValue("CH", "nightMode", cfg_str);


	sprintf(cfg_str, "%f", C_AIMRCS);
	ini.SetValue("CH", "C_AIMRCS", cfg_str);

	sprintf(cfg_str, "%f", C_AIMRCS_R);
	ini.SetValue("CH", "C_AIMRCS_R", cfg_str);

	sprintf(cfg_str, "%f", C_AIMRCS_R_LOCK);
	ini.SetValue("CH", "C_AIMRCS_R_LOCK", cfg_str);

	sprintf(cfg_str, "%d", C_LOCK_ON_LISTED_WEAPONS);
	ini.SetValue("CH", "C_LOCK_ON_LISTED_WEAPONS", cfg_str);

	sprintf(cfg_str, "%d", C_AUTOSPRAY_ON_LISTED_WEAPONS);
	ini.SetValue("CH", "C_AUTOSPRAY_ON_LISTED_WEAPONS", cfg_str);

	sprintf(cfg_str, "%d", C_SCALE_BY_DISTANCE);
	ini.SetValue("CH", "C_SCALE_BY_DISTANCE", cfg_str);

	sprintf(cfg_str, "%d", UseRayTracingLock);
	ini.SetValue("CH", "UseRayTracingLock", cfg_str);

	ini.SetValue("CH", "ScreenWidth", "1920"); // we dont need it anymore, do we?
	ini.SetValue("CH", "ScreenHeight", "1080");

	sprintf(cfg_str, "%f", PlayerFOV);
	ini.SetValue("CH", "PlayerFOV", cfg_str);

	sprintf(cfg_str, "%d", TriggerBotSleepTimeMs);
	ini.SetValue("CH", "TriggerBotSleepTimeMs", cfg_str);

	sprintf(cfg_str, "%d", TriggerBotToggle);
	ini.SetValue("CH", "TriggerBotToggle", cfg_str);

	sprintf(cfg_str, "%d", TriggerBotKeyCode);
	ini.SetValue("CH", "TriggerBotKeyCode", cfg_str);

	sprintf(cfg_str, "%f", ChamsBrightness);
	ini.SetValue("CH", "ChamsBrightness", cfg_str);

	sprintf(cfg_str, "%d", ForceUpdateKeyCode);
	ini.SetValue("CH", "ForceUpdateKeyCode", cfg_str);

	float
		WHcolorX = WHcolor.x,
		WHcolorY = WHcolor.y,
		WHcolorZ = WHcolor.z,
		WHcolorW = WHcolor.w;


	sprintf(cfg_str, "%f", WHcolorX);
	ini.SetValue("CH", "WHcolorX", cfg_str);

	sprintf(cfg_str, "%f", WHcolorY);
	ini.SetValue("CH", "WHcolorY", cfg_str);

	sprintf(cfg_str, "%f", WHcolorZ);
	ini.SetValue("CH", "WHcolorZ", cfg_str);

	sprintf(cfg_str, "%f", WHcolorW);
	ini.SetValue("CH", "WHcolorW", cfg_str);

	float
		ChamscolorX = Chamscolor.x,
		ChamscolorY = Chamscolor.y,
		ChamscolorZ = Chamscolor.z,
		ChamscolorW = Chamscolor.w;

	sprintf(cfg_str, "%f", ChamscolorX);
	ini.SetValue("CH", "ChamscolorX", cfg_str);

	sprintf(cfg_str, "%f", ChamscolorY);
	ini.SetValue("CH", "ChamscolorY", cfg_str);

	sprintf(cfg_str, "%f", ChamscolorZ);
	ini.SetValue("CH", "ChamscolorZ", cfg_str);

	sprintf(cfg_str, "%f", ChamscolorW);
	ini.SetValue("CH", "ChamscolorW", cfg_str);

	for (size_t i = 0; i < 80; ++i) {
		cfg_str[i] = '0';
	}

	for (size_t i = 0; i < 65; ++i) {
		cfg_str[i] = (SprayWeapons[i] ? '1' : '0');
	}
	ini.SetValue("CH", "SprayWeapons", cfg_str);

	for (size_t i = 0; i < 80; ++i) {
		cfg_str[i] = '0';
	}

	for (size_t i = 0; i < 65; ++i) {
		cfg_str[i] = (LockWeapons[i] ? '1' : '0');
	}
	ini.SetValue("CH", "LockWeapons", cfg_str);

	sprintf(cfg_str, "%d", SelectedAimID);
	ini.SetValue("CH", "SelectedAimID", cfg_str);

	sprintf(cfg_str, "%f", NightmodeExposureValue);
	ini.SetValue("CH", "NightmodeExposureValue", cfg_str);

	std::string tmp;

	for (pair<int, std::string> weapon : weaponsnames) {

		tmp = "PaintKit" + weapon.second;
		sprintf(cfg_str, "%d", CONFIG_PaintKit[weapon.first]);
		ini.SetValue("CH", tmp.c_str(), cfg_str);

		tmp = "StatTrak" + weapon.second;
		sprintf(cfg_str, "%d", CONFIG_StatTrak[weapon.first]);
		ini.SetValue("CH", tmp.c_str(), cfg_str);

		tmp = "Quality" + weapon.second;
		sprintf(cfg_str, "%d", CONFIG_Quality[weapon.first]);
		ini.SetValue("CH", tmp.c_str(), cfg_str);

		tmp = "Wear" + weapon.second;
		sprintf(cfg_str, "%f", CONFIG_Wear[weapon.first]);
		ini.SetValue("CH", tmp.c_str(), cfg_str);

		tmp = "Seed" + weapon.second;
		sprintf(cfg_str, "%d", CONFIG_Seed[weapon.first]);
		ini.SetValue("CH", tmp.c_str(), cfg_str);

	}

	ini.SaveFile(configName.c_str());

}

bool checkConfig(std::string configName) { // if it's not a cfg but a random bullshit => we cant load and use it => we dont need to show it in menu

	CSimpleIniA ini;
	ini.SetUnicode();

	ini.LoadFile(configName.c_str());

	std::string cfg_str = "It's wednesday, my dudes!";
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "trigger", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "AIMBOT", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "RCS", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "WallHack", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "Chams", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "antiFlash", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "GrenadePredict", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "skinChanger", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "maphack", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "autoBhop", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "nightMode", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "C_AIMRCS", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "C_AIMRCS_R", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "C_AIMRCS_R_LOCK", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "C_LOCK_ON_LISTED_WEAPONS", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "C_AUTOSPRAY_ON_LISTED_WEAPONS", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "C_SCALE_BY_DISTANCE", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "ScreenWidth", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "ScreenHeight", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "PlayerFOV", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "UseRayTracingLock", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "TriggerBotSleepTimeMs", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "TriggerBotToggle", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "TriggerBotKeyCode", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "ChamsBrightness", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "ForceUpdateKeyCode", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "WHcolorX", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "WHcolorY", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "WHcolorZ", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "WHcolorW", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "ChamscolorX", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "ChamscolorY", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "ChamscolorZ", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "ChamscolorW", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "SprayWeapons", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "LockWeapons", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "SelectedAimID", "error"));
	cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", "NightmodeExposureValue", "error"));

	std::string tmp;

	for (pair<int, std::string> weapon : weaponsnames) {

		tmp = "PaintKit" + weapon.second;

		cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", tmp.c_str(), "error"));

		tmp = "StatTrak" + weapon.second;

		cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", tmp.c_str(), "error"));

		tmp = "Quality" + weapon.second;

		cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", tmp.c_str(), "error"));

		tmp = "Wear" + weapon.second;

		cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", tmp.c_str(), "error"));

		tmp = "Seed" + weapon.second;

		cfg_str = (cfg_str == "error" ? "error" : ini.GetValue("CH", tmp.c_str(), "error"));

	}

	return cfg_str != "error";

}

std::vector<std::string> getConfigsList() { // iterator to 0th element of array, size of array

	vector<std::string> configs;

	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	TCHAR currentDir[256];

	GetCurrentDirectory(256, currentDir);
	StringCchCat(currentDir, 256, "\\*");

	hFind = FindFirstFile(currentDir, &ffd);

	if (hFind == INVALID_HANDLE_VALUE) { // no files in directory
		return configs;
	}

	do {

		std::string currentFileName = ffd.cFileName;

		if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && checkConfig(currentFileName)) {

			configs.push_back(currentFileName);
		}

	} while (FindNextFile(hFind, &ffd) != 0);

	return configs;

}

#pragma endregion

#pragma region Thread_funcs

void VisualsThread(void* parg) {

	while (running) {

		if (WallHack)
			WH();

		if (Chams)
			ChamsFunc();
		else
			game.setModelsBrightness(0.f);

	}

}

void TriggerBotThread(void* parg) {

	while (running) {

		if (trigger)
			TriggerBot();

		if (RCS)
			RecoilControlSystem();

	}

}

void MiscThred(void* parg) {

	while (running) {

		SetFov();

		if (autoBhop)
			BHOP();

		if (maphack)
			MapFunc();

		if (antiFlash)
			AntiFlash();

		if (GrenadePredict)
			game.grenpred(GrenadePredict);

		if (nightMode)
			NightModeBrightnessFix = 1.f / NightmodeExposureValue;
		else
			NightModeBrightnessFix = 1.f;

		NightMode(nightMode);

		if (GetAsyncKeyState(ForceUpdateKeyCode))
			ForceUpdateAll();


	}

}

void SkinChangerThread(void* parg) {

	while (running) {

		if (skinChanger)
			SkinChanger();

	}

}

#pragma endregion

#pragma region GLFWError
static void glfw_error_callback(int error, const char* description) {
	fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}
#pragma endregion

#pragma region RENDER
void render(GLFWwindow* window, ImGuiIO& io, std::vector<std::string>& cfg_data) {
	// Our state
	bool Setup = true;
	bool MousePass = false;
	bool DrawWorkerWindow = false;

	ImVec4 clear_color = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);
	static int item_current = 0;

	HWND hwnd = (HWND)io.ImeWindowHandle;

	glfwSwapInterval(0);

	glfwGetWindowSize(window, &game.ScreenWidth, &game.ScreenHeight);

	// Threads init
	_beginthread(VisualsThread, 0, NULL);
	_beginthread(TriggerBotThread, 0, NULL);
	_beginthread(MiscThred, 0, NULL);
	_beginthread(SkinChangerThread, 0, NULL);

	// Main loop
	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
		ImGui::SetNextWindowPos(ImVec2(0, 0));
		ImGui::Begin("Draw worker", &DrawWorkerWindow, ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoTitleBar);
		ImDrawList* draw_list = ImGui::GetBackgroundDrawList();

		// AIMBOT in main thread

		if (AIMBOT)
			AimBot();

		ImGui::End();
		if (GetAsyncKeyState(VK_INSERT)) {
			MousePass = !MousePass;
			glfwSetWindowAttrib(window, GLFW_MOUSE_PASSTHROUGH, MousePass);
			Sleep(150);
		}

#pragma region MAINWINDOW
		if (!MousePass) {
			ImGui::Begin("Main window", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_AlwaysAutoResize);

			if (ImGui::Button("Load config")) {
				ConfigLoad(configs_list[config_id]);
			}
			ImGui::SameLine();
			if (ImGui::Button("Save config")) {
				ConfigSave(configs_list[config_id]);
			}
			ImGui::SetNextItemWidth(100);

			ImGui::Combo("Select Config", &config_id, configs_list, cfg_data.size());

			if (ImGui::BeginTabBar("##Tabs", ImGuiTabBarFlags_None)) {
				if (ImGui::BeginTabItem("FastMenu")) {
					ImGui::Checkbox("Enable AimBot", &AIMBOT);
					ImGui::Checkbox("Enable TriggerBot", &trigger);
					ImGui::Checkbox("Enable Standalone RCS", &RCS);
					ImGui::Checkbox("Enable WallHack", &WallHack);
					ImGui::Checkbox("Enable Chams", &Chams);
					ImGui::Checkbox("Enable SkinChanger", &skinChanger);
					ImGui::Checkbox("Enable AutoBhop", &autoBhop);
					ImGui::Checkbox("Enable AntiFlash", &antiFlash);
					ImGui::Checkbox("Enable MapHack", &maphack);
					ImGui::Checkbox("Enable GrenadePredict", &GrenadePredict);
					ImGui::Checkbox("Enable NightMode", &nightMode);
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("AimBot and RCS")) {
					ImGui::Checkbox("Enable AimBot", &AIMBOT);
					ImGui::Checkbox("Enable Standalone RCS", &RCS);
					ImGui::Separator();
					ImGui::Combo("AimBot Bone", &SelectedAimID, AimTargets, IM_ARRAYSIZE(AimTargets));
					ImGui::DragFloat("Smooth", &C_AIMRCS, 0.1F, 0, 100);
					ImGui::DragFloat("FOV", &C_AIMRCS_R, 0.1F, 0, 30);
					ImGui::DragFloat("Lock FOV", &C_AIMRCS_R_LOCK, 0.1F, 0, 30);
					ImGui::Checkbox("Scale FOV by distance to target", &C_SCALE_BY_DISTANCE);
					ImGui::Separator();
					ImGui::Checkbox("Lock on listed weapons", &C_LOCK_ON_LISTED_WEAPONS);
					if (C_LOCK_ON_LISTED_WEAPONS) {
						ImGui::SetNextItemOpen(true);
					} else {
						ImGui::SetNextItemOpen(false);
					}
					if (ImGui::TreeNode("Weapons")) {
						ImGui::Columns(3, NULL, false);
						for (auto i : weaponsnames) {
							if (ImGui::Selectable(i.second.c_str(), &LockWeapons[i.first])) {}
							ImGui::NextColumn();
						}
						ImGui::Columns(1);
						ImGui::TreePop();
					}
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("TriggerBot")) {
					ImGui::Checkbox("Enable TriggerBot", &trigger);
					ImGui::Separator();
					std::string keytext;
					if (!TriggerBotKeyCode || TriggerBotKeyCode == 0x3A) {
						keytext = "Press this button to bind key";
					} else {
						keytext = helpers.format("Binded key is %s", Keys[TriggerBotKeyCode]);
					}

					if (TriggerBotKeyCode == 0x3A) {
						ImGui::Text("Press key to bind");
						int key = 0x3A;
						for (int i = 0x08; i < 0x92; i++) {
							if (GetAsyncKeyState(i)) {
								key = i;
							}
						}
						TriggerBotKeyCode = key;
						if (ImGui::Button("Continue without key")) {
							TriggerBotKeyCode = 0;
						}
					} else if (ImGui::Button(keytext.c_str())) {
						TriggerBotKeyCode = 0x3A;
					}
					ImGui::DragInt("TriggerBot delay", &TriggerBotSleepTimeMs, 1.0F, 0, 500);
					ImGui::Checkbox("TriggerBot Toggle", &TriggerBotToggle);
					ImGui::Separator();
					ImGui::Checkbox("Autospray on selected weapon", &C_AUTOSPRAY_ON_LISTED_WEAPONS);
					if (C_AUTOSPRAY_ON_LISTED_WEAPONS) {
						ImGui::SetNextItemOpen(true);
					} else {
						ImGui::SetNextItemOpen(false);
					}
					if (ImGui::TreeNode("Weapons")) {
						ImGui::Columns(3, NULL, false);
						for (auto i : weaponsnames) {
							if (ImGui::Selectable(i.second.c_str(), &SprayWeapons[i.first])) {}
							ImGui::NextColumn();
						}
						ImGui::Columns(1);
						ImGui::TreePop();
					}
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("WallHack")) {
					ImGui::Checkbox("Enable WallHack", &WallHack);
					ImGui::Separator();
					ImGui::ColorPicker3("Color", (float*)&WHcolor);
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("Chams")) {
					ImGui::Checkbox("Enable Chams", &Chams);
					ImGui::Separator();
					ImGui::DragFloat("Brightness", &ChamsBrightness, 0.1F, 0, 10);
					ImGui::ColorPicker3("Color", (float*)&Chamscolor);
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("SkinChanger")) {
					ImGui::Checkbox("Enable SkinChanger", &skinChanger);
					ImGui::Separator();
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("ForceUpdate")) {
					if (ImGui::Button("ForceUpdate")) {
						ForceUpdateAll();
					}
					std::string keytext;
					if (!ForceUpdateKeyCode || ForceUpdateKeyCode == 0x3A) {
						keytext = "Press this button to bind key";
					} else {
						keytext = helpers.format("Binded key is %s", Keys[ForceUpdateKeyCode]);
					}

					if (ForceUpdateKeyCode == 0x3A) {
						ImGui::Text("Press key to bind");
						int key = 0x3A;
						for (int i = 0x08; i < 0x92; i++) {
							if (GetAsyncKeyState(i)) {
								key = i;
							}
						}
						ForceUpdateKeyCode = key;
						if (ImGui::Button("Continue without key")) {
							ForceUpdateKeyCode = 0;
						}
					} else if (ImGui::Button(keytext.c_str())) {
						ForceUpdateKeyCode = 0x3A;
					}
					ImGui::EndTabItem();
				}
				if (ImGui::BeginTabItem("Misc")) {
					ImGui::DragFloat("Weapon FOV", &PlayerFOV, 1.0F, 90, 150);
					ImGui::Separator();
					ImGui::DragFloat("NightmodeExposureValue", &NightmodeExposureValue, 0.001F, 0.f, 1.f);
					ImGui::Separator();
					ImGui::Checkbox("Use new RayTracing lock while flashed", &UseRayTracingLock);
					ImGui::EndTabItem();
				}
				ImGui::EndTabBar();
			}
			ImGui::End();
		}
#pragma endregion
#ifdef DEBUG
		if (DEBUG) {
			ImGui::Begin("DEBUG", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_AlwaysAutoResize);
			ImGui::End();
		}
#endif // !DEBUG

		// Rendering
		ImGui::Render();
		int display_w, display_h;
		glfwGetFramebufferSize(window, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);
		glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(window);

		if (Setup) {
			Setup = false;
			MousePass = true;
			Sleep(1000);
			ShowWindow(hwnd, SW_HIDE);
			Sleep(1000);
			ShowWindow(hwnd, SW_SHOWMAXIMIZED);
			}
	}
}
#pragma endregion

int main(int, char**) {

	// default config


	std::vector<std::string> cfg_data = getConfigsList();

	if (cfg_data.size() == 0) {
		const char defaultConfigName[8] = "cfg.ini";
		configs_list[0] = defaultConfigName;
		InitConfig();
		cfg_data.push_back("cfg.ini");
	}

	for (int i = 0; i < cfg_data.size(); ++i) {
		configs_list[i] = cfg_data[i].c_str();
	}

	ConfigLoad(configs_list[config_id]);

	// Setup window
	glfwSetErrorCallback(glfw_error_callback);
	if (!glfwInit())
		return 1;

	const char* glsl_version = "#version 130";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, 1);
	glfwWindowHint(GLFW_FOCUS_ON_SHOW, 1);
	glfwWindowHint(GLFW_MOUSE_PASSTHROUGH, 1);
	glfwWindowHint(GLFW_FLOATING, 1);
	glfwWindowHint(GLFW_MAXIMIZED, 1);
	glfwWindowHint(GLFW_DECORATED, 0);

	// Create window with graphics context
	GLFWwindow* window = glfwCreateWindow(1920, 1080, "Wednesday", NULL, NULL);

	if (window == NULL)
		return 1;
	glfwMakeContextCurrent(window);


	bool err = gl3wInit() != 0;
	if (err) {
		fprintf(stderr, "Failed to initialize OpenGL loader!\n");
		return 1;
	}

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	// Setup Dear ImGui style
	//ImGui::StyleColorsDark();
	ImGui::StyleColorsClassic();

	// Setup Platform/Renderer backends
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);

	render(window, io, cfg_data);

	// Cleanup
	running = false;
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}
