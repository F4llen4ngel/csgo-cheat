#pragma once
#include <Windows.h>
#include <winioctl.h>
#include <typeinfo>
#include <string>
#include <wchar.h>
#include <locale>
#include <codecvt>

#ifdef DEBUG
#include "VMProtectSDK.h"
#endif // !DEBUG



typedef unsigned char uchar;


/* IOCTL Codes needed for our driver */

// Request to read virtual user memory (memory of a program) from kernel space (aka win RPM)
#define IO_READ_REQUEST CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0701, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to write virtual user memory (memory of a program) from kernel space (aka win WPM)
#define IO_WRITE_REQUEST CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0702, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to retrieve the process id of csgo process from kernel space
#define IO_GET_ID_REQUEST CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0703, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to retrieve the base address of client.dll in csgo.exe from kernel space
#define IO_GET_MODULE_REQUEST_Client CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0704, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to retrieve the base address of engine.dll from kernel space
#define IO_GET_MODULE_REQUEST_Engine CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0705, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Copy file
#define IO_COPY_FILE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0706, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)


#pragma region R/W STRUCTS

// datatype for read request
typedef struct _KERNEL_READ_REQUEST {
	UINT32 MyProcessId;
	UINT64 MyAddress;

	UINT32 TargetProcessId;
	UINT64 TargetAddress;

	UINT32 Size;

} KERNEL_READ_REQUEST, * PKERNEL_READ_REQUEST;

typedef struct _KERNEL_WRITE_REQUEST {
	UINT32 MyProcessId;
	UINT64 MyAddress;

	UINT32 TargetProcessId;
	UINT64 TargetAddress;

	UINT32 Size;

} KERNEL_WRITE_REQUEST, * PKERNEL_WRITE_REQUEST;

typedef struct _KERNEL_MODULE_INFO {
	ULONG Address;
	ULONG Size;

} KERNEL_MODULE_INFO, * PKERNEL_MODULE_INFO;

typedef struct _COPY_FILE_REQUEST {
	wchar_t source[256];
	wchar_t drain[256];

} COPY_FILE_REQUEST, * PCOPY_FILE_REQUEST;

#pragma endregion

struct ModuleCache {
	DWORD	 Entry;
	DWORD    Size;
	BOOL	 Readed;
	uint8_t* Content;
};

// interface for our driver
class DudeInterface {
public:
	HANDLE hDriver; // Handle to driver
	DWORD PID, myPID;
	// Initializer
	DudeInterface() {
		hDriver = CreateFileA("\\\\.\\kerneldude", GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, 0, 0);
		myPID = GetCurrentProcessId();
	}

	template <typename type>
	type RVM(ULONG ReadAddress) {
#ifdef DEBUG
		VMProtectBeginMutation("RVM Memory");
#endif // !DEBUG
		
		type tData;
		UINT64 tDataAddress = (UINT64)&tData;

		directRVM<type>((DWORD)ReadAddress, tDataAddress);

		return tData;
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

	template <typename type>
	bool WVM(ULONG WriteAddress, type WriteValue) {
#ifdef DEBUG
		VMProtectBeginMutation("WVM Memory");
#endif // !DEBUG
		UINT64 tDataAddress = (UINT64)&WriteValue;
		return directWVM<type>((DWORD)WriteAddress, tDataAddress);
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}


	template <typename type>
	bool directRVM(DWORD readAddress, UINT64 returnAddress) {
#ifdef DEBUG
		VMProtectBeginMutation("Direct RVM Memory");
#endif // !DEBUG
		if (hDriver == INVALID_HANDLE_VALUE)
			return false;

		DWORD Bytes;
		KERNEL_READ_REQUEST ReadRequest;

		ReadRequest.MyProcessId = (UINT32)myPID;
		ReadRequest.TargetProcessId = (UINT32)PID;
		ReadRequest.MyAddress = returnAddress;
		ReadRequest.TargetAddress = (UINT64)readAddress;
		ReadRequest.Size = (UINT32)sizeof(type);

		if (DeviceIoControl(hDriver, IO_READ_REQUEST, &ReadRequest, sizeof(ReadRequest), 0, 0, &Bytes, NULL))
			return true;
		else
			return false;
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

	template <typename type>
	bool directWVM(DWORD writeAddress, UINT64 writeValueAddress) {
#ifdef DEBUG
		VMProtectBeginMutation("Direct WVM Memory");
#endif // !DEBUG
		if (hDriver == INVALID_HANDLE_VALUE)
			return false;

		DWORD Bytes;
		KERNEL_READ_REQUEST WriteRequest;

		WriteRequest.MyProcessId = (UINT32)myPID;
		WriteRequest.TargetProcessId = (UINT32)PID;
		WriteRequest.MyAddress = writeValueAddress;
		WriteRequest.TargetAddress = (UINT64)writeAddress;
		WriteRequest.Size = (UINT32)sizeof(type);

		if (DeviceIoControl(hDriver, IO_WRITE_REQUEST, &WriteRequest, sizeof(WriteRequest), 0, 0, &Bytes, NULL))
			return true;
		else
			return false;
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

	bool directRVM(DWORD readAddress, UINT64 returnAddress, UINT32 Size) {
#ifdef DEBUG
		VMProtectBeginMutation("Direct RVM Memory by size");
#endif // !DEBUG
		if (hDriver == INVALID_HANDLE_VALUE)
			return false;

		DWORD Bytes;
		KERNEL_READ_REQUEST ReadRequest;

		ReadRequest.MyProcessId = (UINT32)myPID;
		ReadRequest.TargetProcessId = (UINT32)PID;
		ReadRequest.MyAddress = returnAddress;
		ReadRequest.TargetAddress = (UINT64)readAddress;
		ReadRequest.Size = Size;

		if (DeviceIoControl(hDriver, IO_READ_REQUEST, &ReadRequest, sizeof(ReadRequest), 0, 0, &Bytes, NULL))
			return true;
		else
			return false;
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

	bool directWVM(DWORD writeAddress, UINT64 writeValueAddress, UINT32 Size) {
#ifdef DEBUG
		VMProtectBeginMutation("Direct WVM Memory by size");
#endif // !DEBUG
		if (hDriver == INVALID_HANDLE_VALUE)
			return false;

		DWORD Bytes;
		KERNEL_READ_REQUEST WriteRequest;

		WriteRequest.MyProcessId = (UINT32)myPID;
		WriteRequest.TargetProcessId = (UINT32)PID;
		WriteRequest.MyAddress = writeValueAddress;
		WriteRequest.TargetAddress = (UINT64)writeAddress;
		WriteRequest.Size = Size;

		if (DeviceIoControl(hDriver, IO_WRITE_REQUEST, &WriteRequest, sizeof(WriteRequest), 0, 0, &Bytes, NULL))
			return true;
		else
			return false;
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

	bool CopyFile(std::string source, std::string drain) {
#ifdef DEBUG
		VMProtectBeginMutation("Copy file driver");
#endif // !DEBUG
		if (hDriver == INVALID_HANDLE_VALUE)
			return false;

		DWORD Bytes;
		COPY_FILE_REQUEST CopyRequest;

		std::wstring wsource = std::wstring(source.begin(), source.end());
		std::wstring wdrain = std::wstring(drain.begin(), drain.end());
		wcscpy(CopyRequest.source, wsource.c_str());
		wcscpy(CopyRequest.drain, wdrain.c_str());

		if (DeviceIoControl(hDriver, IO_COPY_FILE, &CopyRequest, sizeof(CopyRequest), 0, 0, &Bytes, NULL))
			return true;
		else
			return false;
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

	DWORD GetTargetPid() {
#ifdef DEBUG
		VMProtectBeginMutation("Get PID driver");
#endif // !DEBUG
		if (hDriver == INVALID_HANDLE_VALUE)
			return false;

		ULONG Id;
		DWORD Bytes;

		if (DeviceIoControl(hDriver, IO_GET_ID_REQUEST, &Id, sizeof(Id),
			&Id, sizeof(Id), &Bytes, NULL)) {
			PID = Id;
			return Id;
		} else
			return false;
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

	KERNEL_MODULE_INFO GetClientModule() {
#ifdef DEBUG
		VMProtectBeginMutation("GetClientModule driver");
#endif // !DEBUG
		if (hDriver == INVALID_HANDLE_VALUE)
			return { 0, 0 };

		KERNEL_MODULE_INFO Request;
		DWORD Bytes;

		if (DeviceIoControl(hDriver, IO_GET_MODULE_REQUEST_Client, &Request, sizeof(Request), &Request, sizeof(Request), &Bytes, NULL))
			return Request;
		else
			return { 0, 0 };
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

	KERNEL_MODULE_INFO GetEngineModule() {
#ifdef DEBUG
		VMProtectBeginMutation("GetEngineModule driver");
#endif // !DEBUG
		if (hDriver == INVALID_HANDLE_VALUE)
			return { 0, 0 };

		KERNEL_MODULE_INFO Request;
		DWORD Bytes;

		if (DeviceIoControl(hDriver, IO_GET_MODULE_REQUEST_Engine, &Request, sizeof(Request), &Request, sizeof(Request), &Bytes, NULL))
			return Request;
		else
			return { 0, 0 };
#ifdef DEBUG
		VMProtectEnd();
#endif // !DEBUG
	}

};
