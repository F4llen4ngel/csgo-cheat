#include "hardware.h"

void hardware::MouseLeft(int sleepTime) {
	INPUT Input = { 0 };
	// left down
	Input.type = INPUT_MOUSE;
	Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
	::SendInput(1, &Input, sizeof(INPUT));
	Sleep(sleepTime);
	// left up
	::ZeroMemory(&Input, sizeof(INPUT));
	Input.type = INPUT_MOUSE;
	Input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
	::SendInput(1, &Input, sizeof(INPUT));
}

void hardware::MouseRight(int sleepTime) {
	INPUT Input = { 0 };
	// right down
	Input.type = INPUT_MOUSE;
	Input.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
	::SendInput(1, &Input, sizeof(INPUT));
	Sleep(sleepTime);
	// right up
	::ZeroMemory(&Input, sizeof(INPUT));
	Input.type = INPUT_MOUSE;
	Input.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
	::SendInput(1, &Input, sizeof(INPUT));
}

void hardware::MouseMove(Vector2 pos) {
	INPUT Input = { 0 };
	Input.type = INPUT_MOUSE;

	Input.mi.dx = (LONG)pos.x;
	Input.mi.dy = (LONG)pos.y;

	// set move cursor directly
	Input.mi.dwFlags = MOUSEEVENTF_MOVE;

	SendInput(1, &Input, sizeof(INPUT));
}


void hardware::MouseMoveAbsolute(Vector2 pos) {
	INPUT Input = { 0 };
	Input.type = INPUT_MOUSE;

	Input.mi.dx = (LONG)pos.x;
	Input.mi.dy = (LONG)pos.y;

	// set move cursor directly
	Input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;

	SendInput(1, &Input, sizeof(INPUT));
}