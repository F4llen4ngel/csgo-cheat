#include "cheat_engine.h"
#include "database.h"
#include "offsets.h"
#include <stdio.h>

union fp_bit_twiddler {
	float f;
	int i;
} q;



cheat_engine::cheat_engine() {
#ifdef RAYTRACE
	DeleteLocalMapFile();
#endif // !RAYTRACE

	GetGameProcessInformation();
	ScanSignatures();

	updateEngineClientState();
	updateLocalPlayer();
	updateEntityList();

#ifdef RAYTRACE
	CopyCurrentMapFile();
	HMODULE RayTracingModule = LoadLibrary("KernelRayTracing.dll");
	if (!RayTracingModule) {
		std::cout << "FATAL ERROR 1!\n";
		exit(2);
	}

	LoadMap = reinterpret_cast<LoadMapFn>(GetProcAddress(RayTracingModule, "LoadMap"));
	Trace = reinterpret_cast<TraceFn>(GetProcAddress(RayTracingModule, "Trace"));

	long hr = LoadMap();
	if (hr) {
		std::cout << "FATAL ERROR 2!\n";
		exit(3);
	}
#endif // RAYTRACE	
}

void cheat_engine::updateEngineClientState() {
	engineClientState = Driver.RVM<DWORD>(Engine + dwClientState);
}

void cheat_engine::isSpotted(DWORD entity) {
	Driver.WVM<BOOL>(entity + m_bSpotted, 1);
}

DWORD cheat_engine::GetActiveViewModel() {
	DWORD activeViewModel = Driver.RVM<DWORD>(localPlayer + m_hViewModel) & 0xfff;
	activeViewModel = Driver.RVM<DWORD>(dwEntityList + (activeViewModel - 1) * 0x10);
	return activeViewModel;
}

void cheat_engine::updateLocalPlayer() {
	updateEngineClientState();
	localPlayer = Driver.RVM<DWORD>(Client + dwLocalPlayer);
	localPlayerID = Driver.RVM<int>(engineClientState + dwClientState_GetLocalPlayer);
}

void cheat_engine::updateEntityList() {
	entityList = Driver.RVM<DWORD>(Client + dwEntityList);
}

int cheat_engine::getHealthByEntity(DWORD entity) {
	return Driver.RVM<int>(entity + m_iHealth);
}

int cheat_engine::getTeamByEntity(DWORD entity) {
	return Driver.RVM<int>(entity + m_iTeamNum);
}

DWORD cheat_engine::GetWeaponBase(DWORD entity, int id) {
	DWORD iCurWeaponAdress;
	if (id != -1)
		iCurWeaponAdress = Driver.RVM<DWORD>(entity + m_hMyWeapons + id * 0x4) & 0xFFF;
	else
		iCurWeaponAdress = Driver.RVM<DWORD>(entity + m_hActiveWeapon) & 0xFFF;
	DWORD m_iBase = Driver.RVM<DWORD>(Client + dwEntityList + (iCurWeaponAdress - 1) * 0x10);
	return m_iBase;
}

int cheat_engine::GetAccountId(DWORD weaponBase) {
	int accountId = Driver.RVM<int>(weaponBase + m_OriginalOwnerXuidLow);
	return accountId;
}

int cheat_engine::GetCurrentPaintKit(DWORD weaponBase) {
	int curPaintKit = Driver.RVM<int>(weaponBase + m_nFallbackPaintKit);
	return curPaintKit;
}

void cheat_engine::force_update_all() {  
	Driver.WVM<int>(engineClientState + 0x174, -1);
}

void cheat_engine::instant_update_skin() {
	DWORD activeViewModel = GetActiveViewModel();
	if (!activeViewModel)
		return;
	Driver.WVM<UINT>(activeViewModel + m_nModelIndex, -1);
}

void cheat_engine::WriteKnifeData(DWORD weaponBase, short itemDef, DWORD modelIndex) {
	Driver.WVM<short>(weaponBase + m_iItemDefinitionIndex, itemDef);
	Driver.WVM<DWORD>(weaponBase + m_nModelIndex, modelIndex);
	Driver.WVM<DWORD>(weaponBase + m_iViewModelIndex, modelIndex);
}

void cheat_engine::WriteSkinData(DWORD weaponBase, UINT PaintKit, int StatTrack, int Wear, int Quality, int AccountID, int Seed) {
	Driver.WVM<UINT>(weaponBase + m_nFallbackPaintKit, PaintKit);
	Driver.WVM<int>(weaponBase + m_nFallbackSeed, Seed);
	Driver.WVM<float>(weaponBase + m_flFallbackWear, Wear);
	if (StatTrack > 0) {
		Driver.WVM<int>(weaponBase + m_nFallbackStatTrak, StatTrack);
	}
	Driver.WVM<int>(weaponBase + m_iAccountID, AccountID);
	Driver.WVM<int>(weaponBase + m_iEntityQuality, Quality);
	Driver.WVM<int>(weaponBase + m_iItemIDHigh, -1);
	//if (NameTag != "") {
	//	const char* tag = NameTag.c_str();
	//	// WriteProcessMemory(m->hProc, (LPVOID)(weaponBase + offsets::m_szCustomName), tag, sizeof(char[161]), 0);
	//	// do the thing on the string above using Driver.WVM
	//}
}

void cheat_engine::grenpred(BOOL value) {
	DWORD xored = value ^ (DWORD)Driver.RVM<DWORD>(Client + cl_grenadepreview);
	Driver.WVM<DWORD>(Client + cl_grenadepreview + 0x30, xored);
}


short cheat_engine::getWeaponIdByEntity(DWORD entity, int id) {
	DWORD m_iBase = GetWeaponBase(entity, id);
	short weaponID = Driver.RVM<short>(m_iBase + m_iItemDefinitionIndex);
	return weaponID;
}

BOOL cheat_engine::isEntityScoped(DWORD entity) {
	return (BOOL)Driver.RVM<BYTE>(entity + m_bIsScoped);
}

DWORD cheat_engine::getEntityInScope(DWORD entity) {
	int crosshair = Driver.RVM<int>(entity + m_iCrosshairId);
	DWORD crosshairEntity = Driver.RVM<DWORD>(Client + dwEntityList + ((crosshair - 1) * dwNextPlayer));
	return crosshairEntity;
}

DWORD cheat_engine::getEntityById(int id) {
	return Driver.RVM<DWORD>(Client + dwEntityList + id * dwNextPlayer);
}

BOOL cheat_engine::getSpotedEntity(DWORD entity, int id, int BoneID, int UseRayTracingLock) {
#ifdef RAYTRACE
	if (millis() < FlashedUntil && UseRayTracingLock) {
		Vector3 LocalPlayerHeadPos = getBonePosByEntity(localPlayer, HEAD);
		Vector3 EntityBonePos = getBonePosByEntity(entity, BoneID);
		return !Trace(LocalPlayerHeadPos, EntityBonePos);
} else {
		DWORD entityPlayerSpotedMask = Driver.RVM<DWORD>(entity + m_bSpottedByMask);
		return entityPlayerSpotedMask & (0x1 << id);
	}
#else
	DWORD entityPlayerSpotedMask = Driver.RVM<DWORD>(entity + m_bSpottedByMask);
	return entityPlayerSpotedMask & (0x1 << id);
#endif // RAYTRACE
}

void cheat_engine::readViewMatrix() {
	Driver.directRVM<ViewMatrix>((DWORD)(Client + dwViewMatrix), (UINT64)&matrix);
}

BOOL cheat_engine::world2screen(Vector3 &vecorigin, Vector2 &vecscreen) {
	readViewMatrix();
	vecscreen.x = matrix[0][0] * vecorigin.x + matrix[0][1] * vecorigin.y + matrix[0][2] * vecorigin.z + matrix[0][3];
	vecscreen.y = matrix[1][0] * vecorigin.x + matrix[1][1] * vecorigin.y + matrix[1][2] * vecorigin.z + matrix[1][3];

	auto fltemp = matrix[3][0] * vecorigin.x + matrix[3][1] * vecorigin.y + matrix[3][2] * vecorigin.z + matrix[3][3];

	if (fltemp < 0.01f)
		return false;

	auto invfltemp = 1.f / fltemp;
	vecscreen.x *= invfltemp;
	vecscreen.y *= invfltemp;

	auto x = ScreenWidth / 2.f;
	auto y = ScreenHeight / 2.f;

	x += 0.5f * vecscreen.x * ScreenWidth + 0.5f;
	y -= 0.5f * vecscreen.y * ScreenHeight + 0.5f;

	vecscreen.x = x;
	vecscreen.y = y;

	return true;
}

Vector3 cheat_engine::getEntityPos(DWORD entity) {
	Vector3 result;
	Driver.directRVM<Vector3>((DWORD)(entity + m_vecOrigin), (UINT64)&result);
	return result;
}


Vector3 cheat_engine::getVecPunch(DWORD entity) {
	Vector3 result;
	Driver.directRVM<Vector3>((DWORD)(entity + m_aimPunchAngle), (UINT64)&result);
	return result;
}


Vector3 cheat_engine::getBonePosByEntity(DWORD entity, int boneId) {
	Vector3 vec;
	DWORD BoneMatrix = Driver.RVM<DWORD>(entity + m_dwBoneMatrix);
	vec.x = Driver.RVM<float>(BoneMatrix + 0x30 * boneId + 0x0C);
	vec.y = Driver.RVM<float>(BoneMatrix + 0x30 * boneId + 0x1C);
	vec.z = Driver.RVM<float>(BoneMatrix + 0x30 * boneId + 0x2C);
	return vec;
}

BOOL cheat_engine::IsInRadius(Vector3 viewAngle, Vector3 dstAngle) {
	Vector3 delta = dstAngle - viewAngle;
	delta.Normalize();
	float dist = sqrt(delta.x * delta.x + delta.y * delta.y);
	return dist < aim_r + 1e-7;
}

float cheat_engine::GetVectorDist(Vector3& a, Vector3& b) {
	return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z));
}

DWORD cheat_engine::GetClosestEnemy(Vector3 viewAngles, Vector3 punchVector, int BoneID, int UseRayTracingLock) {
	float min_dist = FLT_MAX;
	int min_dude = 666;
	for (int i = 0; i < 64; i++) {
		DWORD entity = getEntityById(i);
		if (!entity)
			continue;
		if (getDormantByEntity(entity))
			continue;
		if (!getSpotedEntity(entity, localPlayerID, BoneID, UseRayTracingLock) || getHealthByEntity(entity) <= 0 || (getTeamByEntity(entity) == getTeamByEntity(localPlayer) && !DangerZone))
			continue;

		Vector3 AimAngle = CalcAngle(getEyePositionByEntity(localPlayer), getBonePosByEntity(entity, BoneID)) - punchVector;

		Vector3 delta = AimAngle - viewAngles;
		delta.Normalize();

		float cur_dist = sqrt(delta.x * delta.x + delta.y * delta.y);

		if (cur_dist < min_dist) {
			min_dist = cur_dist + 1e-7;
			min_dude = entity;
		}

	}

	return min_dude;
}

void cheat_engine::resetScreenResolution() {
	TempScreenWidth = ScreenWidth/2;
	TempScreenHeight = ScreenHeight/2;
}


void cheat_engine::makeGlow(int glowCurrentPlayerGlowIndex, float R, float G, float B, float A) {
	int glowObj = Driver.RVM<int>(Client + dwGlowObjectManager);
	Driver.WVM<float>(glowObj + (glowCurrentPlayerGlowIndex * 0x38 + 0x4), R);
	Driver.WVM<float>(glowObj + (glowCurrentPlayerGlowIndex * 0x38 + 0x8), G);
	Driver.WVM<float>(glowObj + (glowCurrentPlayerGlowIndex * 0x38 + 0xC), B);
	Driver.WVM<float>(glowObj + (glowCurrentPlayerGlowIndex * 0x38 + 0x10), A);
	Driver.WVM<int>(glowObj + (glowCurrentPlayerGlowIndex * 0x38 + 0x24), 1);
}

DWORD cheat_engine::getEntityGlowIndex(DWORD entity) {
	return Driver.RVM<int>(entity + m_iGlowIndex);
}

void cheat_engine::setModelColor(DWORD entity, Vector3 rgb) {
	Driver.WVM<uint8_t>(entity + 0x70, (uint8_t)rgb.x);
	Driver.WVM<uint8_t>(entity + 0x71, (uint8_t)rgb.y);
	Driver.WVM<uint8_t>(entity + 0x72, (uint8_t)rgb.z);
}

void cheat_engine::setModelsBrightness(float brightness) {
	DWORD thisPtr = (int)(Engine + model_ambient_min - 0x2c);
	DWORD xored = *(DWORD*)&brightness ^ thisPtr;
	Driver.WVM<int>(Engine + model_ambient_min, xored);
}

void cheat_engine::startSpaying() {
	IsSpraying = true;
	INPUT Input = { 0 };
	// left down
	::ZeroMemory(&Input, sizeof(INPUT));
	Input.type = INPUT_MOUSE;
	Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
	::SendInput(1, &Input, sizeof(INPUT));
}

void cheat_engine::stopSpraying() {
	if (!IsSpraying) return;
	IsSpraying = false;
	INPUT Input = { 0 };
	// left up
	::ZeroMemory(&Input, sizeof(INPUT));
	Input.type = INPUT_MOUSE;
	Input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
	::SendInput(1, &Input, sizeof(INPUT));
}

int cheat_engine::getFlashDuration(DWORD entity) {
	int flashDuration = Driver.RVM<int>(entity + m_flFlashDuration);
	return flashDuration;
}

void cheat_engine::setFlashDuration(DWORD entity, int val) {
	Driver.WVM<int>(entity + m_flFlashDuration, val);
}

int cheat_engine::GetActiveWeaponViewModelID(int ActiveWeapon) {
	Driver.RVM<int>(ActiveWeapon + m_iViewModelIndex);
	return 0;
}

UINT cheat_engine::GetModelIndexByName(const char* modelName) {
	DWORD cstate = Driver.RVM<DWORD>(dwClientState);

	//// CClientState + 0x529C -> INetworkStringTable* m_pModelPrecacheTable
	//DWORD nst = Driver.RVM<DWORD>(cstate + m_dwModelPrecache);

	//// INetworkStringTable + 0x40 -> INetworkStringDict* m_pItems
	//DWORD nsd = Driver.RVM<DWORD>(nst + 0x40);

	//// INetworkStringDict + 0xC -> void* m_pItems
	//DWORD nsdi = Driver.RVM<DWORD>(nsd + 0xC);

	//for (UINT i = 0; i < 1024; i++) {
	//	DWORD nsdi_i = Driver.RVM<DWORD>(nsdi + 0xC + i * 0x34);
	//	char str[128] = { 0 };
	//	if (ReadMemory(hProcess, nsdi_i, str, sizeof(str))) {
	//		if (_stricmp(str, modelName) == 0) {
	//			return i;
	//		}
	//	}
	//}

	// TODO: read char* above using Driver.RVM, idk how to do it.

	return 0;
}
UINT cheat_engine::GetModelIndex(const short itemIndex) {
	UINT ret = 0;
	switch (itemIndex) {
	case weapon_knife:
		ret = GetModelIndexByName("models/weapons/v_knife_default_ct.mdl");
		break;
	case weapon_knife_t:
		ret = GetModelIndexByName("models/weapons/v_knife_default_t.mdl");
		break;
	case weapon_knife_bayonet:
		ret = GetModelIndexByName("models/weapons/v_knife_bayonet.mdl");
		break;
	case weapon_knife_flip:
		ret = GetModelIndexByName("models/weapons/v_knife_flip.mdl");
		break;
	case weapon_knife_gut:
		ret = GetModelIndexByName("models/weapons/v_knife_gut.mdl");
		break;
	case weapon_knife_karambit:
		ret = GetModelIndexByName("models/weapons/v_knife_karam.mdl");
		break;
	case weapon_knife_m9_bayonet:
		ret = GetModelIndexByName("models/weapons/v_knife_m9_bay.mdl");
		break;
	case weapon_knife_tactical:
		ret = GetModelIndexByName("models/weapons/v_knife_tactical.mdl");
		break;
	case weapon_knife_falchion:
		ret = GetModelIndexByName("models/weapons/v_knife_falchion_advanced.mdl");
		break;
	case weapon_knife_survival_bowie:
		ret = GetModelIndexByName("models/weapons/v_knife_survival_bowie.mdl");
		break;
	case weapon_knife_butterfly:
		ret = GetModelIndexByName("models/weapons/v_knife_butterfly.mdl");
		break;
	case weapon_knife_push:
		ret = GetModelIndexByName("models/weapons/v_knife_push.mdl");
		break;
	case weapon_knife_ursus:
		ret = GetModelIndexByName("models/weapons/v_knife_ursus.mdl");
		break;
	case weapon_knife_gypsy_jackknife:
		ret = GetModelIndexByName("models/weapons/v_knife_gypsy_jackknife.mdl");
		break;
	case weapon_knife_stiletto:
		ret = GetModelIndexByName("models/weapons/v_knife_stiletto.mdl");
		break;
	case weapon_knife_widowmaker:
		ret = GetModelIndexByName("models/weapons/v_knife_widowmaker.mdl");
		break;
	case weapon_knife_css:
		ret = GetModelIndexByName("models/weapons/v_knife_css.mdl");
		break;
	case weapon_knife_cord:
		ret = GetModelIndexByName("models/weapons/v_knife_cord.mdl");
		break;
	case weapon_knife_canis:
		ret = GetModelIndexByName("models/weapons/v_knife_canis.mdl");
		break;
	case weapon_knife_outdoor:
		ret = GetModelIndexByName("models/weapons/v_knife_outdoor.mdl");
		break;
	case weapon_knife_skeleton:
		ret = GetModelIndexByName("models/weapons/v_knife_skeleton.mdl");
		break;
	default:
		break;
	}
	return ret;
}

float cheat_engine::scaleAimFOVByDistance(Vector3& pos) {
	Vector3 localpos = getEntityPos(localPlayer);
	float distance = (int)GetVectorDist(localpos, pos);
	if (distance <= 300.0f)
		return 1.0f;
	if (distance >= 1000.0f)
		return 0.5f;
	float dist = distance - 300;
	float oran = dist / 700 * 0.4;
	return 1.0f - oran;
}

bool cheat_engine::isWeaponRifle(short localPlayerWeaponID) {

	if (localPlayerWeaponID == weapon_ak47 || localPlayerWeaponID == weapon_aug || localPlayerWeaponID == weapon_bizon || localPlayerWeaponID == weapon_famas || localPlayerWeaponID == weapon_galil || localPlayerWeaponID == weapon_galilar
		|| localPlayerWeaponID == weapon_m249 || localPlayerWeaponID == weapon_m4a1 || localPlayerWeaponID == weapon_m4a1s || localPlayerWeaponID == weapon_mac10 || localPlayerWeaponID == weapon_mp7 || localPlayerWeaponID == weapon_tmp
		|| localPlayerWeaponID == weapon_mp9 || localPlayerWeaponID == weapon_negev || localPlayerWeaponID == weapon_sg556 || localPlayerWeaponID == weapon_ump45 || localPlayerWeaponID == weapon_mp5navy || localPlayerWeaponID == weapon_p90)
		return true;
	else
		return false;
}

bool cheat_engine::isWeaponSniper(short localPlayerWeaponID) {
	if (localPlayerWeaponID == weapon_awp || localPlayerWeaponID == weapon_scar20 || localPlayerWeaponID == weapon_g3sg1 || localPlayerWeaponID == weapon_ssg08 || localPlayerWeaponID == weapon_scout)
		return true;
	else
		return false;
}
bool cheat_engine::isWeaponPistol(short localPlayerWeaponID) {
	if (localPlayerWeaponID == weapon_cz75_auto || localPlayerWeaponID == weapon_deagle || localPlayerWeaponID == weapon_elite || localPlayerWeaponID == weapon_glock || localPlayerWeaponID == weapon_fiveseven
		|| localPlayerWeaponID == weapon_hkp2000 || localPlayerWeaponID == weapon_r8_revolver || localPlayerWeaponID == weapon_usp || localPlayerWeaponID == weapon_p250 || localPlayerWeaponID == weapon_tec9)
		return true;
	else
		return false;
}
bool cheat_engine::isWeaponKnife(short localPlayerWeaponID) {
	if (localPlayerWeaponID == weapon_knife || localPlayerWeaponID == weapon_knife_t || localPlayerWeaponID == weapon_knifegg || localPlayerWeaponID >= 500)
		return true;

	return false;
}
bool cheat_engine::isWeaponGrenade(short localPlayerWeaponID) {
	if (localPlayerWeaponID == weapon_flashbang || localPlayerWeaponID == weapon_hegrenade || localPlayerWeaponID == weapon_smokegrenade || localPlayerWeaponID == weapon_molotov || localPlayerWeaponID == weapon_incgrenade || localPlayerWeaponID == weapon_decoy)
		return true;

	return false;
}

bool cheat_engine::isWeaponBomb(short localPlayerWeaponID) {
	if (localPlayerWeaponID == weapon_c4)
		return true;

	return false;
}

bool cheat_engine::isWeaponTaser(short localPlayerWeaponID) {
	if (localPlayerWeaponID == weapon_taser)
		return true;

	return false;
}

bool cheat_engine::isWeaponShotgun(short localPlayerWeaponID) {
	if (localPlayerWeaponID == weapon_nova || localPlayerWeaponID == weapon_sawedoff || localPlayerWeaponID == weapon_xm1014 || localPlayerWeaponID == weapon_mag7)
		return true;

	return false;
}

bool cheat_engine::writeCharArray(DWORD addr, char* arr, SIZE_T size) {
	for (SIZE_T i = 0; i < size; i++) {
		BOOL check = Driver.WVM<BYTE>(addr + 0x1 * i, (BYTE)arr[i]);
		if (!check)
			return false;
	}
	return true;
}

void cheat_engine::FindLongestArray(const char* Signature, const char* Mask, int Out[2]) {
	int startIndex = 0,
		endIndex = strlen(Signature),
		maskLen = strlen(Mask);

	for (int i = endIndex; i < maskLen; ++i) {
		if (Mask[i] != 'x')
			continue;

		int count = 0;
		for (int j = i; j < maskLen && Mask[j] == 'x'; ++j)
			++count;

		if (count > endIndex) {
			startIndex = i;
			endIndex = count;
		}

		i += (count - 1);
	}

	Out[0] = startIndex;
	Out[1] = endIndex;
}

DWORD cheat_engine::FindSignature(DWORD ModuleAddr, const char* Signature, const char* Mask, const ptrdiff_t Offset) {
	// EXAMPLE
	// FindSignature(DLL START, PATTERN, MASK, OFFSETS) + extra - DLL START
	// DWORD test = FindSignature(Client, "\x8D\x34\x85\x00\x00\x00\x00\x89\x15\x00\x00\x00\x00\x8B\x41\x08\x8B\x48\x04\x83\xF9\xFF", "xxx????xx????xxxxxxxxx", 3) - Client + 0x4;
	ModuleCache* Cache = nullptr;
	for (size_t i = 0; i < ModulesCache.size(); ++i) {
		if (ModulesCache[i].Entry == ModuleAddr)
			Cache = &ModulesCache[i];
	}

	if (!Cache->Readed) {
		Cache->Content = new uint8_t[Cache->Size];
		Driver.directRVM((DWORD)Cache->Entry, (UINT64)&(Cache->Content[0]), (UINT32)Cache->Size);
		Cache->Readed = true;
	}

	int d[2] = { 0 };
	FindLongestArray(Signature, Mask, d);

	const uint8_t len = (uint8_t)strlen(Mask),
		mbeg = (uint8_t)d[0],
		mlen = (uint8_t)d[1],
		mfirst = (uint8_t)Signature[mbeg];

	bool arrInSig[UCHAR_MAX + 1] = { false };

	for (auto i = mbeg; i < mbeg + mlen; ++i)
		arrInSig[(uint8_t)Signature[i]] = true;

	for (int i = Cache->Size - len; i >= 0; --i) {
		const uint8_t cur = Cache->Content[i];
		bool bInSig = arrInSig[cur];
		bool bSkipped = false;

		while (!bInSig && i > mlen) {
			i -= mlen;
			bInSig = arrInSig[Cache->Content[i]];
			bSkipped = true;
		}

		if (bSkipped) {
			++i;
			continue;
		}

		if (cur != mfirst)
			continue;

		if ((i - mbeg < 0) || (i - mbeg + len > Cache->Size))
			return 0;

		for (int j = 0; j < len; ++j) {
			if (j == mbeg || Mask[j] != 'x')
				continue;

			if (Cache->Content[i - mbeg + j] != (uint8_t)Signature[j])
				break;

			if (j + 1 == len)
				return Offset ? *(uintptr_t*)(Cache->Content + i - mbeg + Offset) : (uintptr_t)Cache->Size + i - mbeg;
		}
	}
	return 0;
}

void cheat_engine::setLocalPlayerFov(int val) {
	Driver.WVM<int>(localPlayer + m_iFOV, val);
}


std::string cheat_engine::getCurrentMapPath() {
	updateEngineClientState();
	char MapDir[104];
	char GameDir[168];
	Driver.directRVM(engineClientState + dwClientState_MapDirectory, (UINT64)&MapDir[0], 104);
	Driver.directRVM(Engine + dwGameDir, (UINT64)&GameDir[0], 168);
	std::string MapDirString = MapDir;
	std::string GameDirString = GameDir;
	return GameDirString + "\\" + MapDirString;
}

void cheat_engine::ScanSignatures() {
	dwClientState = FindSignature(Engine, "\xA1\x00\x00\x00\x00\x33\xD2\x6A\x00\x6A\x00\x33\xC9\x89\xB0", "x????xxxxxxxxxx", 1) - Engine + 0x0;
	dwClientState_GetLocalPlayer = FindSignature(Engine, "\x8B\x80\x00\x00\x00\x00\x40\xC3", "xx????xx", 2) + 0x0;
	dwClientState_IsHLTV = FindSignature(Engine, "\x80\xBF\x00\x00\x00\x00\x00\x0F\x84\x00\x00\x00\x00\x32\xDB", "xx?????xx????xx", 2) + 0x0;
	dwClientState_Map = FindSignature(Engine, "\x05\x00\x00\x00\x00\xC3\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xA1", "x????xxxxxxxxx", 1) + 0x0;
	//dwClientState_MapDirectory = FindSignature(Engine, "\xB8\x00\x00\x00\x00\xC3\x05\x00\x00\x00\x00\xC3", "x????xx????x", 7) + 0x0;
	dwClientState_MaxPlayer = FindSignature(Engine, "\xA1\x00\x00\x00\x00\x8B\x80\x00\x00\x00\x00\xC3\xCC\xCC\xCC\xCC\x55\x8B\xEC\x8A\x45\x08", "x????xx????xxxxxxxxxxx", 7) + 0x0;
	dwClientState_PlayerInfo = FindSignature(Engine, "\x8B\x89\x00\x00\x00\x00\x85\xC9\x0F\x84\x00\x00\x00\x00\x8B\x01", "xx????xxxx????xx", 2) + 0x0;
	dwClientState_State = FindSignature(Engine, "\x83\xB8\x00\x00\x00\x00\x00\x0F\x94\xC0\xC3", "xx?????xxxx", 2) + 0x0;
	dwClientState_ViewAngles = FindSignature(Engine, "\xF3\x0F\x11\x80\x00\x00\x00\x00\xD9\x46\x04\xD9\x05", "xxxx????xxxxx", 4) + 0x0;
	clientstate_delta_ticks = FindSignature(Engine, "\xC7\x87\x00\x00\x00\x00\x00\x00\x00\x00\xFF\x15\x00\x00\x00\x00\x83\xC4\x08", "xx????????xx????xxx", 2) + 0x0;
	clientstate_last_outgoing_command = FindSignature(Engine, "\x8B\x8F\x00\x00\x00\x00\x8B\x87\x00\x00\x00\x00\x41", "xx????xx????x", 2) + 0x0;
	//clientstate_choked_commands = FindSignature(Engine, "\x8B\x87\x00\x00\x00\x00\x41", "xx????x", 2) + 0x0;
	clientstate_net_channel = FindSignature(Engine, "\x8B\x8F\x00\x00\x00\x00\x8B\x01\x8B\x40\x18", "xx????xxxxx", 2) + 0x0;
	dwEntityList = FindSignature(Client, "\xBB\x00\x00\x00\x00\x83\xFF\x01\x0F\x8C\x00\x00\x00\x00\x3B\xF8", "x????xxxxx????xx", 1) - Client + 0x0;
	dwForceAttack = FindSignature(Client, "\x89\x0D\x00\x00\x00\x00\x8B\x0D\x00\x00\x00\x00\x8B\xF2\x8B\xC1\x83\xCE\x04", "xx????xx????xxxxxxx", 2) - Client + 0x0;
	dwForceAttack2 = FindSignature(Client, "\x89\x0D\x00\x00\x00\x00\x8B\x0D\x00\x00\x00\x00\x8B\xF2\x8B\xC1\x83\xCE\x04", "xx????xx????xxxxxxx", 2) - Client + 0xc;
	dwForceBackward = FindSignature(Client, "\x55\x8B\xEC\x51\x53\x8A\x5D\x08", "xxxxxxxx", 287) - Client + 0x0;
	dwForceForward = FindSignature(Client, "\x55\x8B\xEC\x51\x53\x8A\x5D\x08", "xxxxxxxx", 245) - Client + 0x0;
	dwForceJump = FindSignature(Client, "\x8B\x0D\x00\x00\x00\x00\x8B\xD6\x8B\xC1\x83\xCA\x02", "xx????xxxxxxx", 2) - Client + 0x0;
	dwForceLeft = FindSignature(Client, "\x55\x8B\xEC\x51\x53\x8A\x5D\x08", "xxxxxxxx", 465) - Client + 0x0;
	dwForceRight = FindSignature(Client, "\x55\x8B\xEC\x51\x53\x8A\x5D\x08", "xxxxxxxx", 512) - Client + 0x0;
	dwGameDir = FindSignature(Engine, "\x68\x00\x00\x00\x00\x8D\x85\x00\x00\x00\x00\x50\x68\x00\x00\x00\x00\x68", "x????xx????xx????x", 1) - Engine + 0x0;
	dwGameRulesProxy = FindSignature(Client, "\xA1\x00\x00\x00\x00\x85\xC0\x0F\x84\x00\x00\x00\x00\x80\xB8\x00\x00\x00\x00\x00\x74\x7A", "x????xxxx????xx?????xx", 1) - Client + 0x0;
	//dwGetAllClasses = FindSignature(Client, "\xA1\x00\x00\x00\x00\xC3\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xA1\x00\x00\x00\x00\xB9", "x????xxxxxxxxxxxx????x", 1) - Client + 0x0;
	dwGlobalVars = FindSignature(Engine, "\x68\x00\x00\x00\x00\x68\x00\x00\x00\x00\xFF\x50\x08\x85\xC0", "x????x????xxxxx", 1) - Engine + 0x0;
	dwGlowObjectManager = FindSignature(Client, "\xA1\x00\x00\x00\x00\xA8\x01\x75\x4B", "x????xxxx", 1) - Client + 0x4;
	dwInput = FindSignature(Client, "\xB9\x00\x00\x00\x00\xF3\x0F\x11\x04\x24\xFF\x50\x10", "x????xxxxxxxx", 1) - Client + 0x0;
	//dwInterfaceLinkList = FindSignature(Client, "\x8B\x35\x00\x00\x00\x00\x57\x85\xF6\x74\x00\x8B\x7D\x08\x8B\x4E\x04\x8B\xC7\x8A\x11\x3A\x10", "xx????xxxx?xxxxxxxxxxxx", 0) + 0x0;
	dwLocalPlayer = FindSignature(Client, "\x8D\x34\x85\x00\x00\x00\x00\x89\x15\x00\x00\x00\x00\x8B\x41\x08\x8B\x48\x04\x83\xF9\xFF", "xxx????xx????xxxxxxxxx", 3) - Client + 0x4;
	//dwMouseEnable = FindSignature(Client, "\xB9\x00\x00\x00\x00\xFF\x50\x34\x85\xC0\x75\x10", "x????xxxxxxx", 1) - Client + 0x30;
	//dwMouseEnablePtr = FindSignature(Client, "\xB9\x00\x00\x00\x00\xFF\x50\x34\x85\xC0\x75\x10", "x????xxxxxxx", 1) - Client + 0x0;
	dwPlayerResource = FindSignature(Client, "\x8B\x3D\x00\x00\x00\x00\x85\xFF\x0F\x84\x00\x00\x00\x00\x81\xC7", "xx????xxxx????xx", 2) - Client + 0x0;
	dwRadarBase = FindSignature(Client, "\xA1\x00\x00\x00\x00\x8B\x0C\xB0\x8B\x01\xFF\x50\x00\x46\x3B\x35\x00\x00\x00\x00\x7C\xEA\x8B\x0D", "x????xxxxxxx?xxx????xxxx", 1) - Client + 0x0;
	dwSensitivity = FindSignature(Client, "\x81\xF9\x00\x00\x00\x00\x75\x1D\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x44\x24\x00\x8B\x44\x24\x0C\x35\x00\x00\x00\x00\x89\x44\x24\x0C", "xx????xxxxxx????xxxxx?xxxxx????xxxx", 2) - Client + 0x2c;
	dwSensitivityPtr = FindSignature(Client, "\x81\xF9\x00\x00\x00\x00\x75\x1D\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x44\x24\x00\x8B\x44\x24\x0C\x35\x00\x00\x00\x00\x89\x44\x24\x0C", "xx????xxxxxx????xxxxx?xxxxx????xxxx", 2) - Client + 0x0;
	//dwSetClanTag = FindSignature(Engine, "\x53\x56\x57\x8B\xDA\x8B\xF9\xFF\x15", "xxxxxxxxx", 0) + 0x0;
	dwViewMatrix = FindSignature(Client, "\x0F\x10\x05\x00\x00\x00\x00\x8D\x85\x00\x00\x00\x00\xB9", "xxx????xx????x", 3) - Client + 0xb0;
	dwWeaponTable = FindSignature(Client, "\xB9\x00\x00\x00\x00\x6A\x00\xFF\x50\x08\xC3", "x????xxxxxx", 1) - Client + 0x0;
	dwWeaponTableIndex = FindSignature(Client, "\x39\x86\x00\x00\x00\x00\x74\x06\x89\x86\x00\x00\x00\x00\x8B\x86", "xx????xxxx????xx", 2) + 0x0;
	dwYawPtr = FindSignature(Client, "\x81\xF9\x00\x00\x00\x00\x75\x16\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x45\x00\x81\x75\x00\x00\x00\x00\x00\xEB\x0A\x8B\x01\x8B\x40\x30\xFF\xD0\xD9\x5D\x0C\x8B\x55\x08", "xx????xxxxxx????xxxx?xx?????xxxxxxxxxxxxxxx", 2) - Client + 0x0;
	dwZoomSensitivityRatioPtr = FindSignature(Client, "\x81\xF9\x00\x00\x00\x00\x75\x1A\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x45\x00\x8B\x45\xF4\x35\x00\x00\x00\x00\x89\x45\xFC\xEB\x0A\x8B\x01\x8B\x40\x30\xFF\xD0\xD9\x5D\xFC\xA1", "xx????xxxxxx????xxxx?xxxx????xxxxxxxxxxxxxxxx", 2) - Client + 0x0;
	//dwbSendPackets = FindSignature(Engine, "\xB3\x01\x8B\x01\x8B\x40\x10\xFF\xD0\x84\xC0\x74\x0F\x80\xBF\x00\x00\x00\x00\x00\x0F\x84", "xxxxxxxxxxxxxxx?????xx", 0) + 0x1;
	m_pStudioHdr = FindSignature(Client, "\x8B\xB6\x00\x00\x00\x00\x85\xF6\x74\x05\x83\x3E\x00\x75\x02\x33\xF6\xF3\x0F\x10\x44\x24", "xx????xxxxxxxxxxxxxxxx", 2) + 0x0;
	m_yawClassPtr = FindSignature(Client, "\x81\xF9\x00\x00\x00\x00\x75\x16\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x45\x00\x81\x75\x00\x00\x00\x00\x00\xEB\x0A\x8B\x01\x8B\x40\x30\xFF\xD0\xD9\x5D\x0C\x8B\x55\x08", "xx????xxxxxx????xxxx?xx?????xxxxxxxxxxxxxxx", 2) - Client + 0x0;
	m_pitchClassPtr = FindSignature(Client, "\xA1\x00\x00\x00\x00\x89\x74\x24\x28", "x????xxxx", 1) - Client + 0x0;
	m_bDormant = FindSignature(Client, "\x8A\x81\x00\x00\x00\x00\xC3\x32\xC0", "xx????xxx", 2) + 0x8;
	model_ambient_min = FindSignature(Engine, "\xF3\x0F\x10\x0D\x00\x00\x00\x00\xF3\x0F\x11\x4C\x24\x00\x8B\x44\x24\x20\x35\x00\x00\x00\x00\x89\x44\x24\x0C", "xxxx????xxxxx?xxxxx????xxxx", 4) - Engine + 0x0;
	//set_abs_angles = FindSignature(Client, "\x55\x8B\xEC\x83\xE4\xF8\x83\xEC\x64\x53\x56\x57\x8B\xF1\xE8", "xxxxxxxxxxxxxxx", 0) + 0x0;
	//set_abs_origin = FindSignature(Client, "\x55\x8B\xEC\x83\xE4\xF8\x51\x53\x56\x57\x8B\xF1\xE8", "xxxxxxxxxxxxx", 0) + 0x0;
	//is_c4_owner = FindSignature(Client, "\x56\x8B\xF1\x85\xF6\x74\x31", "xxxxxxx", 0) - Client + 0x0;
	//force_update_spectator_glow = FindSignature(Client, "\x74\x07\x8B\xCB\xE8\x00\x00\x00\x00\x83\xC7\x10", "xxxxx????xxx", 0) + 0x0;
	anim_overlays = FindSignature(Client, "\x8B\x89\x00\x00\x00\x00\x8D\x0C\xD1", "xx????xxx", 2) + 0x0;
	//m_flSpawnTime = FindSignature(Client, "\x89\x86\x00\x00\x00\x00\xE8\x00\x00\x00\x00\x80\xBE\x00\x00\x00\x00\x00", "xx????x????xx?????", 2) + 0x0;
	//find_hud_element = FindSignature(Client, "\x55\x8B\xEC\x53\x8B\x5D\x08\x56\x57\x8B\xF9\x33\xF6\x39\x77\x28", "xxxxxxxxxxxxxxxx", 0) + 0x0;
	dwViewmodelFOV = FindSignature(Client, "\xF3\x00\x00\x00\x00\x00\x00\x00\xF3\x0F\x11\x45\xF8\x81\x75\x00\x00\x00\x00\x00\xD9", "x???????xxxxxxx?????x", 4);
}

void cheat_engine::GetGameProcessInformation() {
	ProcessId = Driver.GetTargetPid();
	KERNEL_MODULE_INFO EngineS = Driver.GetEngineModule();
	KERNEL_MODULE_INFO ClientS = Driver.GetClientModule();
	Engine = EngineS.Address;
	EngineSize = EngineS.Size;
	Client = ClientS.Address;
	ClientSize = ClientS.Size;
	ModuleCache tempE, tempC;
	tempE.Entry = Engine;
	tempE.Size = EngineSize;
	tempE.Readed = false;
	tempC.Entry = Client;
	tempC.Size = ClientSize;
	tempC.Readed = false;
	ModulesCache.push_back(tempE);
	ModulesCache.push_back(tempC);
}

void cheat_engine::CopyCurrentMapFile() {
	std::string MapPath = "\\DosDevices\\" + getCurrentMapPath();
	TCHAR buffer[256];
	GetCurrentDirectory(sizeof(buffer), buffer);
	MyDir = std::string(buffer);
	std::string TempMapPath = "\\DosDevices\\" + MyDir + "\\temp.bsp";

	Driver.CopyFile(MapPath, TempMapPath);
}

void cheat_engine::setlLocalPlayerViewModelFov(float val) {

	int tmp = *((int*)&val);

	Driver.WVM<int>(dwViewmodelFOV, tmp ^ (dwViewmodelFOV - 0x2C));

}

//const auto active_weapon = g_ptr_memory->read_memory<ptrdiff_t>(baseaddress_ + netvars::dw_active_weapon);
//return g_ptr_memory->read_memory<ptrdiff_t>(client_module->get_image_base() + offsets::dw_entitylist + ((active_weapon & 0xFFF) - 1) * 0x10);

DWORD cheat_engine::getCurrentWeaponBase(DWORD entity) {
	DWORD active_weapon = Driver.RVM<DWORD>(entity + m_hActiveWeapon);
	DWORD active_weapon_base = Driver.RVM<DWORD>(Client + dwEntityList + ((active_weapon & 0xFFF) - 1) * 0x10);
	return active_weapon_base;
}

int cheat_engine::getTickBase(DWORD entity) {
	return Driver.RVM<int>(entity + m_nTickBase);
}

globalvars_t cheat_engine::getGlobalVars() {
	return Driver.RVM<globalvars_t>(Engine + dwGlobalVars);
}

//static bool can_shoot() {
//	const c_entity local(local_player_index());
//
//	const auto next_primary_attack = g_ptr_memory->read_memory< float >(local.current_weapon_base() + netvars::f_next_primary_attack);
//	const auto server_time = local.tickbase() * get_globalvars().interval_per_tick;
//
//	return (!(next_primary_attack > server_time));
//}

BOOL cheat_engine::canShoot(DWORD entity) {
	DWORD current_weapon_base = getCurrentWeaponBase(entity);
	float next_primary_attack = Driver.RVM<float>(current_weapon_base + m_flNextPrimaryAttack);
	float server_time = getTickBase(entity) * getGlobalVars().interval_per_tick;
	return !(next_primary_attack > server_time);
}

void cheat_engine::send_packet(BOOL status) {
	BYTE value = status ? 1 : 0;
	Driver.WVM<BYTE>(Engine + dwbSendPackets, value);
}

int cheat_engine::get_sequence_number() {
	return Driver.RVM<int>(dwClientState + clientstate_last_outgoing_command);
}

input_t cheat_engine::get_input() {
	return Driver.RVM<input_t>(Client + dwInput);
}

BOOL cheat_engine::getDormantByEntity(DWORD entity) {
	return Driver.RVM<BOOL>(entity + m_bDormant);
}

Vector3 cheat_engine::getViewAngles() {
	DWORD ClientState = Driver.RVM<DWORD>(Engine + dwClientState);
	return Driver.RVM<Vector3>(ClientState + dwClientState_ViewAngles);
}

Vector3 cheat_engine::getPunchAngles(DWORD entity) {
	return Driver.RVM<Vector3>(entity + m_aimPunchAngle);
}

float cheat_engine::getSimulationTime(DWORD entity) {
	return Driver.RVM<float>(entity + m_flSimulationTime);
}

Vector3 cheat_engine::getEyePositionByEntity(DWORD entity) {

	return Driver.RVM<Vector3>(entity + m_vecViewOffset) + Driver.RVM<Vector3>(entity + m_vecOrigin);

}

int cheat_engine::time_to_ticks(float time) {
	return static_cast<int>(static_cast<float>(0.5f) + static_cast<float>(time) / static_cast<float>(getGlobalVars().interval_per_tick));
}

BYTE cheat_engine::getPlayerJumpFlag(DWORD entity) {
	return Driver.RVM<BYTE>(entity + m_fFlags);
}

void cheat_engine::forceJump() {
	Driver.WVM<DWORD>(Client + dwForceJump, 6);
}

void cheat_engine::writeViewAngles(Vector3 Angle) {
	DWORD ClientState = Driver.RVM<DWORD>(Engine + dwClientState);
	Driver.WVM<Vector3>(ClientState + dwClientState_ViewAngles, Angle);
}

//Auto changing spotting type
LONG cheat_engine::millis() {
	SYSTEMTIME time;
	GetSystemTime(&time);
	LONG time_ms = (time.wSecond * 1000) + time.wMilliseconds;
	return time_ms;
}

Vector3 cheat_engine::CalcAngle(const Vector3& src, const Vector3& dst) {
	Vector3 vAngle;
	Vector3 delta = Vector3((src.x - dst.x), (src.y - dst.y), (src.z - dst.z));
	double hyp = sqrt(delta.x * delta.x + delta.y * delta.y);

	vAngle.x = float(atanf(float(delta.z / hyp)) * 57.295779513082f);
	vAngle.y = float(atanf(float(delta.y / delta.x)) * 57.295779513082f);
	vAngle.z = 0.0f;

	if (delta.x >= 0.0)
		vAngle.y += 180.0f;

	return vAngle;
}

int cheat_engine::ShotsFiredByEntity(DWORD entity) {
	return Driver.RVM<int>(entity + m_iShotsFired);
}

int cheat_engine::getClassID(DWORD entity) {
	return Driver.RVM<int>(Driver.RVM<int>(Driver.RVM<int>(Driver.RVM<int>(entity + 0x8) + 0x8) + 0x1) + 0x14);
}

void cheat_engine::activateNightMode(DWORD entity, float NightmodeExposureValue) {
	Driver.WVM<int>(entity + m_bUseCustomAutoExposureMin, 1);
	Driver.WVM<int>(entity + m_bUseCustomAutoExposureMax, 1);
	Driver.WVM<float>(entity + m_flCustomAutoExposureMin, NightmodeExposureValue);
	Driver.WVM<float>(entity + m_flCustomAutoExposureMax, NightmodeExposureValue);
}

void cheat_engine::deactivateNightMode(DWORD entity) {
	Driver.WVM<bool>(entity + m_bUseCustomAutoExposureMin, 0);
	Driver.WVM<bool>(entity + m_bUseCustomAutoExposureMax, 0);
}

bool cheat_engine::DeleteLocalMapFile() {
	TCHAR buffer[256];
	GetCurrentDirectory(sizeof(buffer), buffer);
	MyDir = std::string(buffer);
	std::string TempMapPath = MyDir + "\\temp.bsp";
	DWORD a = GetLastError();
	return (bool)DeleteFile(TempMapPath.c_str());
}
