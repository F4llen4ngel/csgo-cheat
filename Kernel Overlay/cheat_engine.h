#pragma once

#include <iostream>
#include <vector>
#include <comdef.h>
#include <stdlib.h>
#include "database.h"
#include "datatypes.h"
#include "wtypes.h"
#include "Dude.h"

using LoadMapFn = int(__stdcall*) ();
using TraceFn = bool(__stdcall*) (Vector3 start, Vector3 end);

class cheat_engine {
public:
	//funcs
	cheat_engine();
	void init();
	void updateEngineClientState();
	void updateLocalPlayer();
	void updateEntityList();
	int getHealthByEntity(DWORD entity);
	int getTeamByEntity(DWORD entity);
	short getWeaponIdByEntity(DWORD entity, int id = -1);
	BOOL isEntityScoped(DWORD entity);
	DWORD getEntityInScope(DWORD entity); // must be uint
	DWORD getEntityById(int id);
	BOOL getSpotedEntity(DWORD entity, int id, int BoneID, int UseRayTracingLock);
	void readViewMatrix();
	BOOL world2screen(Vector3 &vecOrigin, Vector2 &vecScreen);
	Vector3 getEntityPos(DWORD entity);
	Vector3 getBonePosByEntity(DWORD entity, int boneid);
	BOOL IsInRadius(Vector3 viewAngle, Vector3 dstAngle);
	float GetVectorDist(Vector3& a, Vector3& b);
	DWORD GetClosestEnemy(Vector3 viewAngles, Vector3 punchVector, int BoneID, int UseRayTracingLock);
	Vector3 getVecPunch(DWORD entity);
	void resetScreenResolution();
	void makeGlow(int glowCurrentPlayerGlowIndex, float R, float G, float B, float A);
	DWORD getEntityGlowIndex(DWORD entity);
	void setModelColor(DWORD entity, Vector3 rgb);
	void setModelsBrightness(float brightness);
	void startSpaying();
	void stopSpraying();
	int getFlashDuration(DWORD entity);
	void setFlashDuration(DWORD entity, int val);
	int GetActiveWeaponViewModelID(int ActiveWeapon);
	DWORD GetWeaponBase(DWORD entity, int id = -1);
	int GetAccountId(DWORD weaponBase);
	int GetCurrentPaintKit(DWORD weaponBase);
	void WriteSkinData(DWORD weaponBase, UINT PaintKit, int StatTrack, int Wear, int Quality, int AccountID, int Seed = 420);
	void grenpred(BOOL value = true);
	void force_update_all();
	void instant_update_skin();
	void WriteKnifeData(DWORD weaponBase, short itemDef, DWORD modelIndex);
	void isSpotted(DWORD entity);
	DWORD GetActiveViewModel();
	UINT GetModelIndexByName(const char* modelName);
	UINT GetModelIndex(const short itemIndex);
	float scaleAimFOVByDistance(Vector3& pos);
	bool isWeaponRifle(short localPlayerWeaponID);
	bool isWeaponSniper(short localPlayerWeaponID);
	bool isWeaponPistol(short localPlayerWeaponID);
	bool isWeaponKnife(short localPlayerWeaponID);
	bool isWeaponGrenade(short localPlayerWeaponID);
	bool isWeaponBomb(short localPlayerWeaponID);
	bool isWeaponTaser(short localPlayerWeaponID);
	bool isWeaponShotgun(short localPlayerWeaponID);
	bool writeCharArray(DWORD addr, char* arr, SIZE_T size);
	void FindLongestArray(const char* Signature, const char* Mask, int Out[2]);
	DWORD FindSignature(DWORD ModuleAddr, const char* Signature, const char* Mask, const ptrdiff_t Offset);
	void setLocalPlayerFov(int val);
	std::string getCurrentMapPath();
	void ScanSignatures();
	void GetGameProcessInformation();
	void CopyCurrentMapFile();
	void setlLocalPlayerViewModelFov(float val);
	DWORD getCurrentWeaponBase(DWORD entity);
	int getTickBase(DWORD entity);
	globalvars_t getGlobalVars();
	BOOL canShoot(DWORD entity);
	void send_packet(BOOL status);
	int get_sequence_number();
	input_t get_input();
	BOOL getDormantByEntity(DWORD entity);
	Vector3 getViewAngles();
	Vector3 getPunchAngles(DWORD entity);
	float getSimulationTime(DWORD entity);
	Vector3 getEyePositionByEntity(DWORD entity);
	int time_to_ticks(float time);
	BYTE getPlayerJumpFlag(DWORD entity);
	void forceJump();
	void writeViewAngles(Vector3 Angle);
	LONG millis();
	Vector3 CalcAngle(const Vector3& src, const Vector3& dst);
	int ShotsFiredByEntity(DWORD entity);
	int getClassID(DWORD entity);
	void activateNightMode(DWORD entity, float NightmodeExposureValue);
	void deactivateNightMode(DWORD entity);
	bool DeleteLocalMapFile();
	//vars

	std::vector<ModuleCache> ModulesCache;
	std::string MyDir;


	BOOL DangerZone = false;

	BOOL IMFUCKINGGOD = false;
	BOOL IsSpraying = false;

	DudeInterface Driver;
	ViewMatrix matrix;
	DWORD engineClientState, localPlayer, entityList;
	int localPlayerID, ScreenWidth, ScreenHeight, TempScreenWidth, TempScreenHeight;
	float aim_r = 100;
	float aimBotSmooth = 2;
	Vector3 oldPunch = { 0, 0, 0 };


	LoadMapFn LoadMap;
	TraceFn Trace;

	ULONG FlashedUntil = 0;

	// Main Config
	

private:
	//funcs

	//vars

	DWORD Engine, Client, ProcessId, EngineSize, ClientSize;
	const DWORD dwNextPlayer = 0x10;
};

