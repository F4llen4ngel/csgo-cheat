#pragma once
#include <cmath>
#include <cstdint>
#include <Windows.h>

using namespace std;

struct Vector3 {
	float x, y, z;

	Vector3() {};

	Vector3(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}

	float magnitude() {
		return sqrt(x * x + y * y + z * z);
	}

	float dist(Vector3& to) {
		Vector3 diff = *this - to;
		return diff.magnitude();
	}

	void reset() {
		x = 0;
		y = 0;
		z = 0;
	}

	Vector3 operator-(const Vector3& vec) {
		return { x - vec.x, y - vec.y, z - vec.z };
	}

	Vector3 operator+(const Vector3& vec) {
		return { x + vec.x, y + vec.y, z + vec.z };
	}

	Vector3 operator*(const float& val) {
		return Vector3(x * val, y * val, z * val);
	}

	Vector3 operator/(const float& val) {
		return Vector3(x / val, y / val, z / val);
	}

	float Dot(Vector3& other) {
		return (x * other.x + y * other.y + z * other.z);
	}

	float Length() {
		return sqrt(x * x + y * y + z * z);
	}

	void Normalize() {

		while (x < -180.0f) x += 360.0f;
		while (x > 180.0f) x -= 360.0f;

		while (y < -180.0f) y += 360.0f;
		while (y > 180.0f) y -= 360.0f;

		if (x > 89.0f) x = 89.0f;
		else if (x < -89.0f) x = -89.0f;

		if (y > 180.0f) y = 180.0f;
		else if (y < -180.0f) y = -180.0f;

		z = 0.f;
	}

};


struct Vector2 {
	float x, y;

	const Vector2 operator-(const Vector2& vec) {
		return {x - vec.x, y - vec.y};
	}

	const Vector2 operator+(const Vector2& vec) {
		return {x + vec.x, y + vec.y};
	}
};

struct ViewMatrix {
	float matrix[4][4];
	float* operator[ ](int idx) {
		return matrix[idx];
	}
};

struct globalvars_t {
	float    realtime;
	int      framecount;
	float    absoluteframetime;
	float    absoluteframestarttimestddev;
	float    curtime;
	float    frametime;
	int      maxClients;
	int      tickcount;
	float    interval_per_tick;
	float    interpolation_amount;
	int      simTicksThisFrame;
	int      network_protocol;
	void* pSaveData;
	bool     m_bClient;
	int      nTimestampNetworkingBase;
	int      nTimestampRandomizeWindow;
};

struct input_t {
	char	  pad_0x00[0x0C];             // 0x00
	bool      m_bTrackIRAvailable;          // 0x04
	bool      m_bMouseInitialized;          // 0x05
	bool      m_bMouseActive;               // 0x06
	bool      m_bJoystickAdvancedInit;      // 0x07
	uint8_t   Unk1[44];                   // 0x08
	DWORD m_pKeys;                      // 0x34
	uint8_t   Unk2[100];                  // 0x38
	bool      m_bCameraInterceptingMouse;   // 0x9C
	bool      m_bCameraInThirdPerson;       // 0x9D
	bool      m_bCameraMovingWithMouse;     // 0x9E
	Vector3	  m_vecCameraOffset;            // 0xA0
	bool      m_bCameraDistanceMove;        // 0xAC
	int32_t   m_nCameraOldX;                // 0xB0
	int32_t   m_nCameraOldY;                // 0xB4
	int32_t   m_nCameraX;                   // 0xB8
	int32_t   m_nCameraY;                   // 0xBC
	bool      m_bCameraIsOrthographic;      // 0xC0
	Vector3	  m_vecPreviousViewAngles;      // 0xC4
	Vector3	  m_vecPreviousViewAnglesTilt;  // 0xD0
	float     m_flLastForwardMove;          // 0xDC
	int32_t   m_nClearInputState;           // 0xE0
	uint8_t   Unk3[0x8];                  // 0xE4
	DWORD m_pCommands;                  // 0xEC
	DWORD m_pVerifiedCommands;          // 0xF0
};

struct usercmd_t {
	DWORD pVft;                // 0x00
	int32_t   m_iCmdNumber;        // 0x04
	int32_t   m_iTickCount;        // 0x08
	Vector3    m_vecViewAngles;     // 0x0C
	Vector3    m_vecAimDirection;   // 0x18
	float     m_flForwardmove;     // 0x24
	float     m_flSidemove;        // 0x28
	float     m_flUpmove;          // 0x2C
	int32_t   m_iButtons;          // 0x30
	uint8_t   m_bImpulse;          // 0x34
	uint8_t   Pad1[3];
	int32_t   m_iWeaponSelect;     // 0x38
	int32_t   m_iWeaponSubtype;    // 0x3C
	int32_t   m_iRandomSeed;       // 0x40
	int16_t   m_siMouseDx;         // 0x44
	int16_t   m_siMouseDy;         // 0x46
	bool      m_bHasBeenPredicted; // 0x48
	uint8_t   Pad2[27];
}; //0x64

struct verified_usercmd_t {
	usercmd_t m_Command;
	uint32_t  m_Crc;
};

struct backtrack_data_t {
	float simtime;
	Vector3 hitboxvector;
};