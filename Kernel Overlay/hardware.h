#pragma once
#include <windows.h>
#include <winuser.h>
#include "datatypes.h"
#include <future>

class hardware {
public:
	void MouseLeft(int sleepTime = 10);
	void MouseRight(int sleepTime = 10);
	void MouseMove(Vector2 pos);
	void MouseMoveAbsolute(Vector2 pos);
};

