﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using BetterExternalRaytrace;
using RGiesecke.DllExport;
using System.Runtime.InteropServices;

namespace ExportedCodeSolution
{
    public class BTrace
    {
        static BSPFile bsp;

        [DllExport(ExportName = "LoadMap", CallingConvention = CallingConvention.StdCall)]
        public static int LoadMap()
        {
            try
            {
                using (var fileStream = File.OpenRead("temp.bsp"))
                    bsp = new BSPFile(fileStream);
                return 0;
            }
            catch (Exception e)
            {
                return e.HResult;
            }

        }

        [DllExport(ExportName = "Trace", CallingConvention = CallingConvention.StdCall)]
        public static bool Trace(Vector3 start, Vector3 end)
        {
            return bsp.RayTrace(start, end, out Vector3 hit);
        }
    }
}