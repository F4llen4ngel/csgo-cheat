#include "ntos.h"
#include "ntifs.h"
#include <ntddk.h>
#include <wdf.h>
#include <windef.h>
#include <wdm.h>
#include "ldr_data_table.h"


//#define DEBUG

DRIVER_INITIALIZE DriverEntry;


// Request to read virtual user memory (memory of a program) from kernel space (aka win RPM)
#define IO_READ_REQUEST CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0701, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to write virtual user memory (memory of a program) from kernel space (aka win WPM)
#define IO_WRITE_REQUEST CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0702, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to retrieve the process id of csgo process from kernel space
#define IO_GET_ID_REQUEST CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0703, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to retrieve the base address of client.dll in csgo.exe from kernel space
#define IO_GET_MODULE_REQUEST_Client CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0704, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to retrieve the base address of engine.dll from kernel space
#define IO_GET_MODULE_REQUEST_Engine CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0705, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

// Request to copy file from kernel space
#define IO_COPY_FILE CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0706, METHOD_BUFFERED, FILE_SPECIAL_ACCESS)

#pragma region VARS

PDEVICE_OBJECT pDeviceObject; // Our driver object
UNICODE_STRING dev, dos; // Driver registry paths

ULONG csgoId, Client, Engine;
ULONG ClientSize, EngineSize;

PDRIVER_OBJECT dumm3;


typedef struct _KLDR_DATA_TABLE_ENTRY {
	LIST_ENTRY InLoadOrderLinks;
	PVOID ExceptionTable;
	ULONG ExceptionTableSize;
	//ULONG padding1;
	PVOID GpValue;
	PVOID NonPagedDebugInfo;
	PVOID DllBase;
	PVOID EntryPoint;
	ULONG SizeOfImage;
	UNICODE_STRING FullDllName;
	UNICODE_STRING BaseDllName;
	ULONG Flags;
	USHORT LoadCount;
	USHORT __Unused5;
	PVOID SectionPointer;
	ULONG CheckSum;
	//ULONG Padding2;
	PVOID LoadedImports;
	PVOID PatchInformation;
} KLDR_DATA_TABLE_ENTRY, * PKLDR_DATA_TABLE_ENTRY;

#pragma endregion

#pragma region R/W STRUCTS

// datatype for read request
typedef struct _KERNEL_READ_REQUEST {
	UINT32 MyProcessId;
	UINT64 MyAddress;

	UINT32 TargetProcessId;
	UINT64 TargetAddress;

	UINT32 Size;

} KERNEL_READ_REQUEST, * PKERNEL_READ_REQUEST;

typedef struct _KERNEL_WRITE_REQUEST {
	UINT32 MyProcessId;
	UINT64 MyAddress;

	UINT32 TargetProcessId;
	UINT64 TargetAddress;

	UINT32 Size;

} KERNEL_WRITE_REQUEST, * PKERNEL_WRITE_REQUEST;

typedef struct _KERNEL_MODULE_INFO {
	ULONG Address;
	ULONG Size;

} KERNEL_MODULE_INFO, * PKERNEL_MODULE_INFO;

typedef struct _COPY_FILE_REQUEST {
	wchar_t source[256];
	wchar_t drain[256];

} COPY_FILE_REQUEST, * PCOPY_FILE_REQUEST;

#pragma endregion

NTSTATUS UnloadDriver(PDRIVER_OBJECT pDriverObject);
NTSTATUS CreateCall(PDEVICE_OBJECT DeviceObject, PIRP irp);
NTSTATUS CloseCall(PDEVICE_OBJECT DeviceObject, PIRP irp);

#pragma region HIDDING

VOID HideDriver(PDRIVER_OBJECT pDriverObject) {
	KIRQL irql = KeRaiseIrqlToDpcLevel(); // raises IRQL to DISPATCH level
	PLDR_DATA_TABLE_ENTRY prevEntry, nextEntry, modEntry;
	modEntry = (PLDR_DATA_TABLE_ENTRY)pDriverObject->DriverSection;
	prevEntry = (PLDR_DATA_TABLE_ENTRY)modEntry->InLoadOrderLinks.Blink;
	nextEntry = (PLDR_DATA_TABLE_ENTRY)modEntry->InLoadOrderLinks.Flink;
	// changing pointers of previous and next process in the list to point around our Driver process
	// this means our Driver process will be skipped when a different program tries enumerating the list
	prevEntry->InLoadOrderLinks.Flink = modEntry->InLoadOrderLinks.Flink;
	nextEntry->InLoadOrderLinks.Blink = modEntry->InLoadOrderLinks.Blink;
	// changing our flink and blink pointers to point to our Driver process
	// this is done just to not have any loose ends and accidentally blue screen
	modEntry->InLoadOrderLinks.Flink = (PLIST_ENTRY)modEntry;
	modEntry->InLoadOrderLinks.Blink = (PLIST_ENTRY)modEntry;
	// once everything is done we lower the IRQL level back to normal
	KeLowerIrql(irql);
}


NTSTATUS DelDriverFile() {
	PUNICODE_STRING pUsDriverPath = NULL;
	pUsDriverPath = &((PKLDR_DATA_TABLE_ENTRY)dumm3->DriverSection)->FullDllName;
	IO_STATUS_BLOCK IoStatusBlock;
	HANDLE FileHandle;
	OBJECT_ATTRIBUTES ObjectAttributes;
	InitializeObjectAttributes(
		&ObjectAttributes,
		pUsDriverPath,
		OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE,
		0,
		0);

	NTSTATUS Status = IoCreateFileEx(&FileHandle,
		SYNCHRONIZE | DELETE,
		&ObjectAttributes,
		&IoStatusBlock,
		NULL,
		FILE_ATTRIBUTE_NORMAL,
		FILE_SHARE_DELETE,
		FILE_OPEN,
		FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT,
		NULL,
		0,
		CreateFileTypeNone,
		NULL,
		IO_NO_PARAMETER_CHECKING,
		NULL);

	if (!NT_SUCCESS(Status)) {
		//DbgPrintEx(0, 0, "IoCreateFileEx ERROR\n");
		return Status;
	}

	PFILE_OBJECT FileObject;
	Status = ObReferenceObjectByHandleWithTag(FileHandle,
		SYNCHRONIZE | DELETE,
		*IoFileObjectType,
		KernelMode,
		'eliF',
		(PVOID*)(&FileObject),
		NULL);
	if (!NT_SUCCESS(Status)) {
		ObCloseHandle(FileHandle, KernelMode);
		return Status;
	}

	const PSECTION_OBJECT_POINTERS SectionObjectPointer = FileObject->SectionObjectPointer;
	SectionObjectPointer->ImageSectionObject = NULL;

	// call MmFlushImageSection, make think no backing image and let NTFS to release file lock
	CONST BOOLEAN ImageSectionFlushed = MmFlushImageSection(SectionObjectPointer, MmFlushForDelete);

	ObfDereferenceObject(FileObject);
	ObCloseHandle(FileHandle, KernelMode);

	if (ImageSectionFlushed) {
		// chicken fried rice
		Status = ZwDeleteFile(&ObjectAttributes);
		if (NT_SUCCESS(Status)) {
			return Status;
		}
	}
	return Status;
}

#pragma endregion

#pragma region DISKOP

ULONG GetFileSize(PUNICODE_STRING File) {
	NTSTATUS			FileNtStatus = STATUS_SUCCESS;
	OBJECT_ATTRIBUTES	FileobjAttr;
	HANDLE				Filehandle;
	IO_STATUS_BLOCK		FileioStatusBlock;

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Try to GetFileSize %wZ\n", File);
#endif // DEBUG

	InitializeObjectAttributes(&FileobjAttr, File,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
		NULL, NULL);

	FileNtStatus = ZwCreateFile(&Filehandle,
		GENERIC_READ,
		&FileobjAttr, &FileioStatusBlock,
		NULL,
		FILE_ATTRIBUTE_NORMAL,
		0,
		FILE_OPEN,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 0);

	FILE_STANDARD_INFORMATION fileInfo = { 0 };

	if (NT_SUCCESS(FileNtStatus)) {
#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Success ZwCreateFile file %wZ\n", File);
#endif // DEBUG
		FileNtStatus = ZwQueryInformationFile(
			Filehandle,
			&FileioStatusBlock,
			&fileInfo,
			sizeof(fileInfo),
			FileStandardInformation
		);
		if (NT_SUCCESS(FileNtStatus)) {
#ifdef DEBUG
			DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Success ZwQueryInformationFile file %wZ\n", File);
#endif // DEBUG
		} else {
#ifdef DEBUG
			DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][ERROR] Failed to ZwQueryInformationFile file %wZ, Error is %d\n", File, FileNtStatus);
#endif // DEBUG
		}
		ZwClose(Filehandle);
	} else {
#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][ERROR] Failed to ZwCreateFile file %wZ, Error is %d\n", File, FileNtStatus);
#endif // DEBUG
	}

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] GetFileSize is done. Path: %wZ, Error code is %d, Result is %d\n", File, FileNtStatus, (ULONG)fileInfo.EndOfFile.QuadPart);
#endif // DEBUG

	return (ULONG)fileInfo.EndOfFile.QuadPart;
}

ULONG TouchFile(PUNICODE_STRING File) {
	NTSTATUS			FileNtStatus = STATUS_SUCCESS;
	OBJECT_ATTRIBUTES	FileobjAttr;
	HANDLE				Filehandle;
	IO_STATUS_BLOCK		FileioStatusBlock;

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Try to TouchFile %wZ\n", File);
#endif // DEBUG

	InitializeObjectAttributes(&FileobjAttr, File,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
		NULL, NULL);

	FileNtStatus = ZwCreateFile(&Filehandle,
		GENERIC_READ,
		&FileobjAttr, &FileioStatusBlock,
		NULL,
		FILE_ATTRIBUTE_NORMAL,
		0,
		FILE_OPEN_IF,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 0);

	if (NT_SUCCESS(FileNtStatus)) {
#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Success ZwCreateFile file %wZ\n", File);
#endif // DEBUG
	} else {
#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][ERROR] Failed to ZwCreateFile file %wZ, Error is %d\n", File, FileNtStatus);
#endif // DEBUG
	}

	ZwClose(Filehandle);

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] TouchFile is done. Path: %wZ, Error code is %d\n", File, FileNtStatus);
#endif // DEBUG

	return FileNtStatus;
}

NTSTATUS ReadFile(PUNICODE_STRING File, PBYTE arr, ULONG Size) {
	NTSTATUS			FileNtStatus = STATUS_SUCCESS;
	OBJECT_ATTRIBUTES	FileobjAttr;
	HANDLE				Filehandle;
	IO_STATUS_BLOCK		FileioStatusBlock;
	LARGE_INTEGER		FilebyteOffset;

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Try to ReadFile %wZ, Size is %d\n", File, Size);
#endif // DEBUG

	InitializeObjectAttributes(&FileobjAttr, File,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
		NULL, NULL);

	FileNtStatus = ZwCreateFile(&Filehandle,
		GENERIC_READ,
		&FileobjAttr, &FileioStatusBlock,
		NULL,
		FILE_ATTRIBUTE_NORMAL,
		FILE_SHARE_READ,
		FILE_OPEN,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 0);

	if (NT_SUCCESS(FileNtStatus)) {
#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Success ZwCreateFile file %wZ\n", File);
#endif // DEBUG
		FilebyteOffset.LowPart = FilebyteOffset.HighPart = 0;
		FileNtStatus = ZwReadFile(Filehandle, NULL, NULL, NULL, &FileioStatusBlock, arr, Size, &FilebyteOffset, NULL);
		if (NT_SUCCESS(FileNtStatus)) {
#ifdef DEBUG
			DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Success ZwReadFile file %wZ\n", File);
#endif // DEBUG
		} else {
#ifdef DEBUG
			DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][ERROR] Failed to ZwReadFile file %wZ, Error is %d\n", File, FileNtStatus);
#endif // DEBUG
		}
	} else {
#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][ERROR] Failed to ZwCreateFile file %wZ, Error is %d\n", File, FileNtStatus);
#endif // DEBUG
	}
	ZwClose(Filehandle);

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] ReadFile is done. Path: %wZ, Error code is %d\n", File, FileNtStatus);
#endif // DEBUG

	return FileNtStatus;
}

NTSTATUS WriteFile(PUNICODE_STRING File, PBYTE arr, ULONG Size) {
	NTSTATUS			FileNtStatus = STATUS_SUCCESS;
	OBJECT_ATTRIBUTES	FileobjAttr;
	HANDLE				Filehandle;
	IO_STATUS_BLOCK		FileioStatusBlock;

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Try to WriteFile %wZ, Size is %d\n", File, Size);
#endif // DEBUG

	InitializeObjectAttributes(&FileobjAttr, File,
		OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
		NULL, NULL);

	FileNtStatus = ZwCreateFile(&Filehandle,
		GENERIC_WRITE,
		&FileobjAttr, &FileioStatusBlock,
		NULL,
		FILE_ATTRIBUTE_NORMAL,
		0,
		FILE_OPEN,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 0);

	if (NT_SUCCESS(FileNtStatus)) {
#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Success ZwCreateFile file %wZ\n", File);
#endif // DEBUG
		FileNtStatus = ZwWriteFile(Filehandle, NULL, NULL, NULL, &FileioStatusBlock, arr, Size, NULL, NULL);
		if (NT_SUCCESS(FileNtStatus)) {
#ifdef DEBUG
			DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] Success ZwWriteFile file %wZ\n", File);
#endif // DEBUG
		} else {
#ifdef DEBUG
			DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][ERROR] Failed to ZwWriteFile file %wZ, Error is %d\n", File, FileNtStatus);
#endif // DEBUG
		}
	} else {
#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][ERROR] Failed to ZwCreateFile file %wZ, Error is %d\n", File, FileNtStatus);
#endif // DEBUG
	}
	ZwClose(Filehandle);

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE_DISKOP][INFO] WriteFile is done. Path: %wZ, Error code is %d\n", File, FileNtStatus);
#endif // DEBUG

	return FileNtStatus;
}
#pragma endregion

#pragma region MEMORYOP

NTSTATUS RVM(PEPROCESS MyProcess, PVOID SourceAddress, PEPROCESS TargetProcess, PVOID TargetAddress, SIZE_T Size) {
	PSIZE_T Bytes;
	if (NT_SUCCESS(MmCopyVirtualMemory(TargetProcess, TargetAddress, MyProcess, SourceAddress, Size, KernelMode, &Bytes)))
		return STATUS_SUCCESS;
	else
		return STATUS_ACCESS_DENIED;
}

NTSTATUS WVM(PEPROCESS MyProcess, PVOID SourceAddress, PEPROCESS TargetProcess, PVOID TargetAddress, SIZE_T Size) {
	PSIZE_T Bytes;
	if (NT_SUCCESS(MmCopyVirtualMemory(MyProcess, SourceAddress, TargetProcess, TargetAddress, Size, KernelMode, &Bytes)))
		return STATUS_SUCCESS;
	else
		return STATUS_ACCESS_DENIED;
}
#pragma endregion


#pragma region PECALLBACK

// set a callback for every PE image loaded to user memory
// then find the client.dll & csgo.exe using the callback
PLOAD_IMAGE_NOTIFY_ROUTINE ImageLoadCallback(PUNICODE_STRING FullImageName,
	HANDLE ProcessId, PIMAGE_INFO ImageInfo) {

	if (wcsstr(FullImageName->Buffer, L"\\bin\\engine.dll")) {
		// if it matches

#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_MODULE][INFO] Loaded engine: %ls \n", FullImageName->Buffer);
		DbgPrintEx(0, 0, "[KERNELDUDE_MODULE][INFO] Engine size: %d \n", ImageInfo->ImageSize);
		DbgPrintEx(0, 0, "[KERNELDUDE_MODULE][INFO] Loaded To Process: %d \n", ProcessId);
#endif // !DEBUG

		Engine = ImageInfo->ImageBase;
		EngineSize = ImageInfo->ImageSize;
		csgoId = ProcessId;
	}


	if (wcsstr(FullImageName->Buffer, L"\\csgo\\bin\\client.dll")) {
		// if it matches

#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_MODULE][INFO] Loaded client: %ls \n", FullImageName->Buffer);
		DbgPrintEx(0, 0, "[KERNELDUDE_MODULE][INFO] Client size: %d \n", ImageInfo->ImageSize);
		DbgPrintEx(0, 0, "[KERNELDUDE_MODULE][INFO] Loaded To Process: %d \n", ProcessId);
#endif // !DEBUG

		Client = ImageInfo->ImageBase;
		ClientSize = ImageInfo->ImageSize;
		csgoId = ProcessId;
	}
	

#ifndef DEBUG
	if (NT_SUCCESS(DelDriverFile())) {
		DbgPrintEx(0, 0, "[KERNELDUDE_HIDE][INFO] Suicide\n");
	}
#endif // !DEBUG

}

#pragma endregion

#pragma region IO
// IOCTL Call Handler function
NTSTATUS IoControl(PDEVICE_OBJECT DeviceObject, PIRP Irp) {
	NTSTATUS Status;
	ULONG BytesIO = 0;

	PIO_STACK_LOCATION stack = IoGetCurrentIrpStackLocation(Irp);

	// Code received from user space
	ULONG ControlCode = stack->Parameters.DeviceIoControl.IoControlCode;

	if (ControlCode == IO_READ_REQUEST) {
		// Get the input buffer & format it to our struct
		PKERNEL_READ_REQUEST ReadInput = (PKERNEL_READ_REQUEST)Irp->AssociatedIrp.SystemBuffer;

		PEPROCESS MyProcess;
		PEPROCESS TargetProcess;
		// Get our process
		if (NT_SUCCESS(PsLookupProcessByProcessId(ReadInput->MyProcessId, &MyProcess)) && NT_SUCCESS(PsLookupProcessByProcessId(ReadInput->TargetProcessId, &TargetProcess)))
			RVM(MyProcess, (PVOID)ReadInput->MyAddress, TargetProcess, (PVOID)ReadInput->TargetAddress, ReadInput->Size);

#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_READ][INFO] Read Params:  %lu, %#010x \n", ReadInput->TargetProcessId, ReadInput->TargetAddress);
#endif // DEBUG

		Status = STATUS_SUCCESS;
		BytesIO = sizeof(KERNEL_READ_REQUEST);
	} else if (ControlCode == IO_WRITE_REQUEST) {
		// Get the input buffer & format it to our struct
		PKERNEL_WRITE_REQUEST WriteInput = (PKERNEL_WRITE_REQUEST)Irp->AssociatedIrp.SystemBuffer;

		PEPROCESS MyProcess;
		PEPROCESS TargetProcess;
		// Get our process
		if (NT_SUCCESS(PsLookupProcessByProcessId(WriteInput->MyProcessId, &MyProcess)) && NT_SUCCESS(PsLookupProcessByProcessId(WriteInput->TargetProcessId, &TargetProcess)))
			WVM(MyProcess, (PVOID)WriteInput->MyAddress, TargetProcess, (PVOID)WriteInput->TargetAddress, WriteInput->Size);

#ifdef DEBUG
		DbgPrintEx(0, 0, "[KERNELDUDE_WRITE][INFO] Write Params:  %lu, %#010x \n", WriteInput->TargetProcessId, WriteInput->TargetAddress);
#endif // DEBUG

		Status = STATUS_SUCCESS;
		BytesIO = sizeof(KERNEL_WRITE_REQUEST);
	} else if (ControlCode == IO_GET_ID_REQUEST) {
		PULONG OutPut = (PULONG)Irp->AssociatedIrp.SystemBuffer;
		*OutPut = csgoId;
		Status = STATUS_SUCCESS;
		BytesIO = sizeof(*OutPut);
	} else if (ControlCode == IO_GET_MODULE_REQUEST_Client) {
		PKERNEL_MODULE_INFO OutPut = (PKERNEL_MODULE_INFO)Irp->AssociatedIrp.SystemBuffer;
		*OutPut = (KERNEL_MODULE_INFO){ Client, ClientSize };
		Status = STATUS_SUCCESS;
		BytesIO = sizeof(*OutPut);
	} else if (ControlCode == IO_GET_MODULE_REQUEST_Engine) {
		PKERNEL_MODULE_INFO OutPut = (PKERNEL_MODULE_INFO)Irp->AssociatedIrp.SystemBuffer;
		*OutPut = (KERNEL_MODULE_INFO){ Engine, EngineSize };
		Status = STATUS_SUCCESS;
		BytesIO = sizeof(*OutPut);
	} else if (ControlCode == IO_COPY_FILE) {
		PCOPY_FILE_REQUEST Input = (PCOPY_FILE_REQUEST)Irp->AssociatedIrp.SystemBuffer;
		UNICODE_STRING Source;
		RtlInitUnicodeString(&Source, Input->source);
		UNICODE_STRING Drain;
		RtlInitUnicodeString(&Drain, Input->drain);

		ULONG Size = GetFileSize(&Source);

		PBYTE arr;
		arr = (PBYTE)ExAllocatePoolWithTag(NonPagedPool, (SIZE_T)Size, 2281337);
		NTSTATUS FileOpStatus = ReadFile(&Source, arr, Size);
		if (NT_SUCCESS(FileOpStatus)) {
			FileOpStatus = TouchFile(&Drain);
			FileOpStatus = WriteFile(&Drain, arr, Size);
		}
		ExFreePoolWithTag(arr, 2281337);
		Status = STATUS_SUCCESS;
		BytesIO = sizeof(*Input);
	} else {
		// if the code is unknown
		Status = STATUS_INVALID_PARAMETER;
		BytesIO = 0;
	}


	// Complete the request
	Irp->IoStatus.Status = Status;
	Irp->IoStatus.Information = BytesIO;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return Status;
}
#pragma endregion

#pragma region ENTRY
// Driver Entrypoint
NTSTATUS DriverEntry(PDRIVER_OBJECT pDriverObject,
	PUNICODE_STRING pRegistryPath) {

	dumm3 = (PDRIVER_OBJECT)pDriverObject;

#ifndef DEBUG
	HideDriver(pDriverObject);
	if (NT_SUCCESS(DelDriverFile())) {
		DbgPrintEx(0, 0, "[KERNELDUDE_HIDE][INFO] Suicide\n");
	}
#endif // !DEBUG

#ifdef DEBUG
	DbgPrintEx(0, 0, "[KERNELDUDE][INFO] Driver Loaded\n");
#endif // DEBUG


	PsSetLoadImageNotifyRoutine(ImageLoadCallback);

	RtlInitUnicodeString(&dev, L"\\Device\\kerneldude");
	RtlInitUnicodeString(&dos, L"\\DosDevices\\kerneldude");

	IoCreateDevice(pDriverObject, 0, &dev, FILE_DEVICE_UNKNOWN, FILE_DEVICE_SECURE_OPEN, FALSE, &pDeviceObject);
	IoCreateSymbolicLink(&dos, &dev);

	pDriverObject->MajorFunction[IRP_MJ_CREATE] = CreateCall;
	pDriverObject->MajorFunction[IRP_MJ_CLOSE] = CloseCall;
	pDriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = IoControl;
	pDriverObject->DriverUnload = UnloadDriver;

	pDeviceObject->Flags |= DO_DIRECT_IO;
	pDeviceObject->Flags &= ~DO_DEVICE_INITIALIZING;


	return STATUS_SUCCESS;
}
#pragma endregion


NTSTATUS UnloadDriver(PDRIVER_OBJECT pDriverObject) {
	PsRemoveLoadImageNotifyRoutine(ImageLoadCallback);
	IoDeleteSymbolicLink(&dos);
	IoDeleteDevice(pDriverObject->DeviceObject);
}

NTSTATUS CreateCall(PDEVICE_OBJECT DeviceObject, PIRP irp) {
	irp->IoStatus.Status = STATUS_SUCCESS;
	irp->IoStatus.Information = 0;

	IoCompleteRequest(irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS CloseCall(PDEVICE_OBJECT DeviceObject, PIRP irp) {
	irp->IoStatus.Status = STATUS_SUCCESS;
	irp->IoStatus.Information = 0;

	IoCompleteRequest(irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}
